#include <Servo.h>

//Initalize servos
Servo myServo1;
Servo myServo2;
Servo myServo3;
Servo myServo4;
Servo myServo5;

float noise = 10;

float prev1 = 0;
float prev2 = 0;
float prev3 = 0;
float prev4 = 0;
float prev5 = 0;

//Set initial positions of each motor, determined with calibration
float initialPos1 = 110;
float initialPos2 = 90;
float initialPos3 = 120;
float initialPos4 = 90;
float initialPos5 = 110;

//Set the position variables equal to initial positions
float pos1 = initialPos1;
float pos2 = initialPos2;
float pos3 = initialPos3;
float pos4 = initialPos4;
float pos5 = initialPos5;

//Create variables to keep track of averages, which are then used
//to assign position every 10 cycles, reducing the noise
//experienced by the Arduino
float avg1;
float avg2;
float avg3;
float avg4;
float avg5;
//Keep track of the number of times that the program has cycled,
//to determine when the motor will move
int count;
//Use a counter to determine the number of times that the "button"
//has experienced a peak
int btnCount = 0;
float pressureSensor = 0;

//Set threshold pressure sensor values
float deadManThreshold = 700;
float graspThreshold = 500;

void setup() {
  //Begin serial monitor
  Serial.begin(9600);
  //Assign input pin modes
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);
  //Assign each servo to a finger
  myServo1.attach(8); //thumb
  myServo2.attach(9); // middle
  myServo3.attach(10); // pinky
  myServo4.attach(11); // index
  myServo5.attach(12); //ring
}

void loop() {

  //Check for a peak in the pressure sensor input, every
  //100ms
  pressureSensor = analogRead(A5);

  if (pressureSensor > deadManThreshold)
  {
    while (true) {
      pressureSensor = analogRead(A5);
      if (pressureSensor < deadManThreshold)
      {
        btnCount = 1;
        delay(10);
        break;
      }
    }
  }

  if (btnCount == 1)
  {
    //While the pressure sensor has not been pressed
    //for a second time
    while (true)
    {
      //If it's above the threshold, then give a brief
      //delay so that the first check doesn't get
      //triggered by accident and break
      pressureSensor = analogRead(A5);
      if (pressureSensor > deadManThreshold)
      {
        while (true) {
          pressureSensor = analogRead(A5);
          if (pressureSensor < deadManThreshold)
          {
            btnCount = 0;
            break;
          }
        }
      }
      //If the button has not been pressed again, then
      //map the input signals to servo positions. These
      //values were all determined through calibration
      pos1 = map(analogRead(A0), 70, 150, 20, 90); //Thumb
      pos2 = map(analogRead(A3), 125, 250, 40, 90); //Middle
      pos3 = map(analogRead(A4), 150, 270, 170, 120); //Pinky
      pos4 = map(analogRead(A1), 150, 260, 145, 90); //Index
      pos5 = map(analogRead(A2), 130, 240, 160, 110); //Ring

      //Increase the count, and add the current position
      //to a running total for each finger
      count ++;
      avg1 += pos1;
      avg2 += pos2;
      avg3 += pos3;
      avg4 += pos4;
      avg5 += pos5;
      //Every ten cycles (the count is set to 1 every
      //10 cycles)
      delay(5);
      if (count % 11 == 0)
      {
        //Calculate the average position for the cycle
        pos1 = avg1 / 10;
        pos2 = avg2 / 10;
        pos3 = avg3 / 10;
        pos4 = avg4 / 10;
        pos5 = avg5 / 10;

        //Write that position to the servos
        if (pos1 > prev1 + noise || pos1 < prev1 - noise)
        {
          myServo1.write(initialPos1);
        }
        if (pos2 > prev2 + noise || pos2 < prev2 - noise)
        {
          myServo2.write(pos2);
        }
        if (pos3 > prev3 + noise || pos3 < prev3 - noise)
        {
          myServo3.write(pos3);
        }
        if (pos4 > prev4 + noise || pos4 < prev4 - noise)
        {
          myServo4.write(pos4);
        }
        if (pos5 > prev5 + noise || pos5 < prev5 - noise)
        {
          myServo5.write(pos5);
        }
        //Reset the averages and counter to be used
        //for the next cycle
        count = 1;
        avg1 = 0;
        avg2 = 0;
        avg3 = 0;
        avg4 = 0;
        avg5 = 0;
        prev1 = pos1;
        prev2 = pos2;
        prev3 = pos3;
        prev4 = pos4;
        prev5 = pos5;
        break;
      }
    }
  }

  //If the button has not been pressed (the machine
  //isn't "on")
  
    else
    {
    //Keep the motors at their original positions
    pos1 = initialPos1;
    pos2 = initialPos2;
    pos3 = initialPos3;
    pos4 = initialPos4;
    pos5 = initialPos5;
    myServo1.write(pos1);
    myServo2.write(pos2);
    myServo3.write(pos3);
    myServo4.write(pos4);
    myServo5.write(pos5);
    }
}
