#include <Servo.h>

Servo myServo1;
Servo myServo2;
Servo myServo3;
Servo myServo4;
Servo myServo5;

float initialPos = 90;

float pos1 = initialPos;
float pos2 = initialPos;
float pos3 = initialPos;
float pos4 = initialPos;
float pos5 = initialPos;

float maximum = 160;
float minimum = 20;

float threshold = 500;

void setup() {
  Serial.begin(9600);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);

  myServo1.attach(8);
  myServo2.attach(9);
  myServo3.attach(10);
  myServo4.attach(11);
  myServo5.attach(12);
}

void loop() {

  float  pressure = analogRead(A5); // for the pressure sensor

  if (pressure > threshold)
  {
    pos1 = map(analogRead(A0),175,300,minimum,maximum);
    pos2 = map(analogRead(A1),90,270,minimum,maximum);
    pos3 = map(analogRead(A2),125,300,minimum,maximum);
    pos4 = map(analogRead(A3),160,360,minimum,maximum);
    pos5 = map(analogRead(A4),80,170,minimum,maximum);
  }

  if (pressure < threshold)
  {
    pos1 = initialPos;
    pos2 = initialPos;
    pos3 = initialPos;
    pos4 = initialPos;
    pos5 = initialPos;    
  }
  
  myServo1.write(pos1);
  myServo2.write(pos2);
  myServo3.write(pos3);
  myServo4.write(pos4);
  myServo5.write(pos5);
  
  delay(300);

}
