#include <Servo.h>

Servo myServo3;
Servo myServo4;
Servo myServo5;

float initialPos = 90;

float pos3 = initialPos;
float pos4 = initialPos;
float pos5 = initialPos;

float maximum = 160;
float minimum = 20;

float threshold = 500;

void setup() {
  Serial.begin(9600);

  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);

  myServo3.attach(10);
  myServo4.attach(11);
  myServo5.attach(12);
}

void loop() {

  
    pos3 = map(analogRead(A2),125,300,minimum,maximum);
    pos4 = map(analogRead(A3),160,360,minimum,maximum);
    pos5 = map(analogRead(A4),80,170,minimum,maximum);
  

  myServo3.write(pos3);
  myServo4.write(pos4);
  myServo5.write(pos5);
  
  delay(150);

}
