# Undergraduate Hand Controller

*Note, over the years I didn't happen to keep the final code for the project, but a sample script employed part way through development has been pushed to this repo.*

## About

This was and old project which comprised of the design and prototyping of a robotic hand. The hand is controlled via a sensory glove, which employs flex sensors to perform position control. 

<img src="images/Glove.jpeg" alt="Ventillator_interface" width="250"/>

*Glove controller for hand manipulator.*


The hand is a 3D printed model, with two joints for each finger. The joints are sprung in place by elastic bands. The fingers are actuated via servo motors which rotate a spool attached to a wire rigidly attached to the finger. Each finger can be individually activated, but both joints of a finger are actuated at once.

<img src="images/Hand.jpg" alt="Ventillator_interface" width="250"/>

*3D printed hand.*


See videos in "videos" folder for demonstrations.