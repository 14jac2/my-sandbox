// Standard C++
#include <stdio.h>
#include <iostream>
using namespace std;

// OpenCV Imports
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp> // OpenCV Core Functionality
#include <opencv2/highgui/highgui.hpp> // High-Level Graphical User Interface
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <math.h>       /* sqrt */


// [Optional] Use OpenCV namespace
// NOTE! If not included, all OpenCV functions will need to be prepended with "cv::"
// EX: "Mat image;" -> "cv:Mat image;"
using namespace cv;

int main(int argc, char** argv)
{
	////////////////////////////////
	/* TEST CODE FOR PRE LAB */
	////////////////////////////////

	/****** TASK 1 ******/
	// Read videos
	VideoCapture capbg("belt_bg.wmv");
	if (!capbg.isOpened())
	{
		//error in opening video of background
		cerr << "Unable to open: " << "belt_bg.wmv" << endl;
		return 0;
	}
	int numframesbg = int(capbg.get(CAP_PROP_FRAME_COUNT));
	double fpsbg = double(capbg.get(CAP_PROP_FPS));

	VideoCapture capfg("belt_fg.wmv");
	if (!capfg.isOpened())
	{
		//error in opening video of foreground
		cerr << "Unable to open: " << "belt_fg.wmv" << endl;
		return 0;
	}
	int numframesfg = int(capfg.get(CAP_PROP_FRAME_COUNT));
	double fpsfg = double(capfg.get(CAP_PROP_FPS));

	// Show videos
	while (1)
	{
		Mat frame;
		capbg >> frame;

		if (frame.empty())
			break;

		imshow("Frame", frame);

		char c = (char)waitKey(1000 / fpsbg); //display at fps
		if (c == 27)
			break;
	}
	destroyAllWindows();

	while (1)
	{
		Mat frame;
		capfg >> frame;

		if (frame.empty())
			break;

		imshow("Frame", frame);

		char c = (char)waitKey(1000 / fpsfg);
		if (c == 27)
			break;
	}
	destroyAllWindows();

	/******* TASK 2 ***********/
	int i = 3; // size of averaging filter

	// Show smoothed video
	while (1)
	{
		Mat frame;
		capbg >> frame;

		if (frame.empty())
			break;

		blur(frame, frame, Size(i, i)); 

		imshow("Frame", frame);

		char c = (char)waitKey(1000 / fpsbg);
		if (c == 27)
			break;
	}
	destroyAllWindows();

	// Find mean
	Mat sumdiffs, mean, stdvsqd;
	Mat sum = Mat::zeros(frame.rows, frame.cols, CV_32FC3);

	while (1)
	{
		Mat frame;
		capbg >> frame;

		if (frame.empty())
			break;

		blur(frame, frame, Size(i, i)); 

		accumulate(frame, sum);
	}
	mean = sum / numframesbg;

	// Find std
	while (1)
	{
		Mat frame, diff;
		capbg >> frame;

		if (frame.empty())
			break;

		blur(frame, frame, Size(i, i)); 

		diff = frame - mean;

		accumulateSquare(diff, sumdiffs);
	}
	stdvsqd = sumdiffs / numframesbg;

	// Subtract background
	float thresh = 1;
	while (1)
	{
		Mat frame;
		capfg >> frame;

		for (int r = 0; r < frame.rows; r++)
		{
			for (int c = 0; c < frame.cols; c++)
			{
				float stdv = sqrt(stdvsqd.at<float>(r, c));
				if (abs(frame.at<float>(r, c) - mean.at<float>(r, c)) > (thresh * stdv))
				{
					frame.at<uchar>(r, c) = (uchar)255;
				}
				else
				{
					frame.at<uchar>(r, c) = (uchar)0;
				}
			}
		}
		imshow("Frame", frame);
			char c = (char)waitKey(1000 / fpsfg); //display at fps, but taking into account the above code this will be larger
		if (c == 27)
			break;
	}


	// Wait for a keystroke in the window
	// NOTE! If you don't add this the window will close immediately!
	cv::waitKey();

	// Terminate the program

	return 0;
}
