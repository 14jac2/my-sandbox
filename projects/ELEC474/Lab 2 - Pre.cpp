/****** TASK 1 ******/
// Read videos
VideoCapture capbg(parser.get<String>("belt_bg.wmv"));
if (!capbg.isOpened())
{
    //error in opening video of background
    cerr << "Unable to open: " << parser.get<String>("belt_bg.wmv") << endl;
    return 0;
}
int numframesbg = int(capbg.get(CAP_PROP_FRAME_COUNT));
double fpsbg = double(capbg.get(CV_CAP_PROP_FPS));

VideoCapture capfg(parser.get<String>("belt_fg.wmv"));
if (!capfg.isOpened())
{
    //error in opening video of foreground
    cerr << "Unable to open: " << parser.get<String>("belt_fg.wmv") << endl;
    return 0;
}
int numframesfg = int(capfg.get(CAP_PROP_FRAME_COUNT));
double fpsfg = double(capfg.get(CV_CAP_PROP_FPS));

// Show videos
while (1)
{
    Mat frame;
    capbg >> frame;

    if (frame.empty())
        break;

    imshow("Frame", frame);

    char c = (char)waitKey(1000/fpsbg); //display at fps
    if (c == 27)
        break;
}
destroyAllWindows();

while (1)
{
    Mat frame;
    capfg >> frame;

    if (frame.empty())
        break;

    imshow("Frame", frame);

    char c = (char)waitKey(1000/fpsfg);
    if (c == 27)
        break;
}
destroyAllWindows();

/******* TASK 2 ***********/
// BLUR IMAGE
// smoothbg = VideoWriter();
int i = 3; // size of averaging filter
Mat sum, sumdiffs, mean, std;

while (1)
{
    Mat frame;
    capbg >> frame;

    if (frame.empty())
        break;

    //FILTER
    blur(frame, frame, Size(i,i)); /*****THIS MIGHT NOT OVERWRITE ORIGINAL FRAME********/

    // New shit
    accumulate(frame, sum);

}

// Show smoothed video
// while (1)
// {
//     Mat frame;
//     capbg >> frame;

//     if (frame.empty())
//         break;

//     imshow("Frame", frame);

//     char c = (char)waitKey(1000/fpsbg);
//     if (c == 27)
//         break;
// }
// destroyAllWindows();

// Find mean
// Mat sum, sumdiffs, mean, std;
// while (1)
// {
//     Mat frame;
//     capbg >> frame;

//     if (frame.empty())
//         break;

//     accumulate(frame, sum);
// }
mean = sum/numframesbg;

// Find std
while (1)
{
    Mat frame, diff;
    capbg >> frame;

    if (frame.empty())
        break;
        
    blur(frame, frame, Size(i,i)); /*****THIS MIGHT NOT OVERWRITE ORIGINAL FRAME********/

    diff = frame - mean;

    accumulateSquare(diff, sumdiffs);
}
std = sqrt(sumdiffs/numframesbg);

// Subtract background
float thresh = 1;
while (1)
{
    Mat frame;
    capfg >> frame;

    for (int r = 0, r < frame.rows, r++)
    {
        for (int c = 0, c < frame.cols, c++)
        {
            if ( abs(frame.at<float>(r,c) - mean.at<float>(r,c)) > (thresh*std.at<float>(r,c))
            {
                frame.at<uchar>(r,c) = 255;
            }
            else
            {
                frame.at<uchar>(r,c) = 0;
            }
        }
    }
    imshow("Frame", frame);
    char c = (char)waitKey(1000/fpsfg); //display at fps, but taking into account the above code this will be larger
    if (c == 27)
        break;
}

