# My Masters



## About

This repo is a pointer to my Master's publications. Also included is a video showcasing the system used for testing.

Find my publications at:
https://qspace.library.queensu.ca/handle/1974/29872
and
https://ieeexplore.ieee.org/document/9636755