# YGK Modular Ventilator
<img src="images/YGK_vent_logo.png" alt="YGK Modular Ventillator Logo" width="300"/>

*YGK Modular Ventillator.*


## About

This project was the International Code Live Challenge initiated during the COVID-19 pandemic. Our team of students designed a low-cost, accessible and easy-to-maintain ventilator for developing countries whose health systems were burdened by the pandemic. Our solution made it to the semi-finals stage and was one of the top 9 teams from over 1000 submissions from 94 countries.

The ventilator code is housed on a seperate Queen's-based repository. The ventilator employed two CPAP machines in sequence to generate airflow and pressure. The airlow/pressure was then externally controlled via servo controlled levers which squeezed the airways closed. The control system was a simple position controller for each lever, controlled at constant rates.

My role in this project was to help design and build the electrical and software control systems.

The interface to the system was a LCD touchpad, which enabled control and monitoring of timings.

<img src="images/Vent_interface.jpg" alt="Ventillator_interface" width="350"/>

*Ventiallator interface.*

See "videos" folder for demonstrations of the system working.

## Code
*Disclaimer - most of the code was not written by me.*
