float Pressure, Pressure2, V_in = 0.0;
int Pressure_Pin_Input = A5;    // select the input pin for the potentiometer

void setup() {
    //pinMode(A1, OUTPUT);
    //analogWrite(A1, 0.0);
    Serial.begin(115200);
}

void loop() {
  // read the value from the sensor:
  //analogWrite(A0, 100);
  V_in = analogRead(Pressure_Pin_Input);
  V_in =V_in*3.3/(pow(2,10)-1) ;//*5./12.;
  Pressure = V_in*25 - 39.19      ; // Equation for 0.3 PSI transducer
  Pressure2 = V_in*94.31 - 147.2      ; // Equation for 1.0 PSI transducer
 
  Serial.println(V_in);
  // stop the program for <sensorValue> milliseconds:
  //delay(10);
  //delayMicroseconds(1000);
}
