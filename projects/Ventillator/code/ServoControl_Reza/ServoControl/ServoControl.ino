/*
 Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>

 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
*/
#include <Servo.h>

Servo InspiratoryValve;  // create servo object to control a servo

int    Breathsrate =15;                            // number of breaths per min
double InhaleTimeRatio =0.5 ;                         //Ratio of inhalation time to total Breath time
//double ExhaleTimeRatio =1-InhaleTimeRatio;       //Ratio of exhalation time to total Breath time
int    T_step=50;                               // Number of point in each breath perod.
double BreathPeriod = 60. *1000./ Breathsrate; // This is each breath time in (ms)
double TimeResolution = BreathPeriod/T_step;       // This is temporal resolution of the signal
int    ValveOpen=95;         // Calibrate this number with each valve
int    ValveClose=5;         // Calibrate this number with each valve
unsigned long t,T1=0;

double InhaleStart=0;    // This is the time in each period that patient start to inhale
double ExhaleStart=InhaleTimeRatio*BreathPeriod;    // This is the time in each period that patient start to exhale
double TB=0;


void setup() {
  InspiratoryValve.attach(9);  // attaches the servo on pin 9 to the servo object
}

void loop() {

  t = millis();
  T1 = (unsigned long int)(t / (TimeResolution)) % int(BreathPeriod / TimeResolution);    // Very important command line. This is converting the realtime in CPU ("t") to the element index in the Reference signal. Time_Step multiplied to 1000 to convert it to micro second because "t" is in micro s.
  TB=T1*TimeResolution;   // TB is actual time in (ms) during each breath and varies from 0 to Breath period.
 if (TB >= InhaleStart && TB < ExhaleStart) {
  InspiratoryValve.write(ValveOpen);
 }
 if (TB > ExhaleStart) {
  InspiratoryValve.write(ValveClose);
 }
//  val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
//  val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
                    // sets the servo position according to the scaled value
  delay(TimeResolution);                           // waits for the servo to get there
  Serial.print(TB);
  //Serial.print(InhaleStart);
  //Serial.println(ExhaleStart);
}
