/*
* ReceiceMultipleFieldsBinarytoFile_P
* portIndex must be set to the port connected to the Arduino
* based on ReceiceMultipleFieldsBinarytoFile, tis version saves data to file
* Press any key to stop logging and save file
*/
 
import processing.serial.*;
import java.util.*;
import java.text.*;
 
PrintWriter output;
DateFormat fnameFormat= new SimpleDateFormat("yyMMdd_HHmm");
DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
String fileName;
 
Serial myPort;    // Create object from Serial Class
short portIndex = 4;  // Select the com port, 0is the first port
char HEADER = 'H';
 
void setup()
{
  size(200, 200);
  //Open whatever serial port is connected to Arduino.
  String portName = Serial.list()[portIndex];
  println(Serial.list());
  println(" Connecting to -> " + Serial.list()[portIndex]);
  myPort = new Serial(this, portName, 115200);
  Date now = new Date();
  fileName = fnameFormat.format(now);
  output = createWriter(fileName + ".csv"); // save the file in the sketch folder
}
 
void draw()
{
  int val;
  String time;
 
  if ( myPort.available() >= 15)    // wait for the entire message to arrive
  {
    if ( myPort.read() == HEADER)    // is this the Header
    {
      String timeString = timeFormat.format(new Date());
      output.print(timeString);
      val = readArduinoInt(); 
      // read but don't output the digital values
 
      // output the six analogues values delimited by a coma
      for(int i=0; i < 6; i ++){
        val = readArduinoInt();
        output.print("," + val);
      }
      output.println();
    }
  }
}
 
void keyPressed() {
  output.flush();  // Writes the remaining data to the file
  output.close();  // Finishes the file
  exit();  // Stops the program
}
 
// return the integer value from bytes received on the serial port
// (in low,high order)
int readArduinoInt()
{
  int val;    // Data received from the serial port
 
  val = myPort.read();    // read the least significant byte
  val = myPort.read() * 256 + val;  // add the most signifcant byte
  return val;
}
