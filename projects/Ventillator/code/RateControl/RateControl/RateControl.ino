#include <Servo.h>

// Pressure reading
float Pressure_03PSI, Pressure_1PSI, V_in, V_in_sum, Analog_in = 0;
const int avg_size = 20; // Determines moving average size
float V_in_Buffer[avg_size]; // Holds memory for moving average
float V_in_Buffer03[avg_size]; // Holds memory for moving average
int iteration03 = 0;
int iteration = 0; // Counter for moving average
int Pressure03_Pin_Input = A5; // Pressure sensor pin
int Pressure1_Pin_Input = A4; // Pressure sensor pin
float Analog_to_Voltage = 3.3/(pow(2,10)-1); // Convert signal to voltage
float offset03 = 0;
float offset1 = 0;
int first = 1;
float Pressure03_total, Pressure1_total = 0;
int offtset_avg = 10000;

float v_alarm = 0; // True or False, thrown when pressure exceeds thresholds (peak or minimum)
float pressure_inhale_peak = 1000; // Expected peak pressure value during patient inhalation
float pressure_inhale_low = 1000; // Expected low pressure value during patient inhalation
float pressure_exhale_peak = 1000; // Expected peak pressure value during patient exhalation
float pressure_exhale_low = 1000; // Expected low pressure value during patient exhalation
float pressure_allowance = 0.1; // Percentage to calculate threshold for peak and low pressure to avoid throwing alarm due to noise
float pressure_inhale_max = pressure_inhale_peak + pressure_inhale_peak*pressure_allowance; // Absolute maximum pressure value allowed during patient inhalation, this is expected peak + threshold for noise
float pressure_inhale_min = pressure_inhale_low - pressure_inhale_low*pressure_allowance; // Absolute minimum pressure value allowed during patient inhalation, this is expected low - threshold for noise
float pressure_exhale_max = pressure_exhale_peak + pressure_exhale_peak*pressure_allowance; // Absolute maximum pressure value allowed during patient exhalation, this is expected peak + threshold for noise
float pressure_exhale_min = pressure_exhale_low - pressure_exhale_low*pressure_allowance; // Absolute minimum pressure value allowed during patient exhalation, this is expected low - threshold for noise

// Valve writing servo
Servo InspiratoryValve; // Servo object for valve control
int InspiratoryValve_Pin_Output = 11; // Servo pin out

// Temporary variables for testing
int Breathrate = 15; // Number of breaths per minute
double InhaleTimeRatio = 0.5; // Ratio of inhalation time to total breath time

// Rate constants
int T_step = 50; // Number of points in each breath
double BreathPeriod = 60.*1000./Breathrate; // Breath time in (ms)
double TimeResolution = BreathPeriod/T_step; // Temporal resolution of signal
int ValveOpen = 130; // Valve specific calibration for open position
int ValveClose = 190; // Valve specific calibration for closed position
unsigned long t, T1 = 0;

// Breath tracking
double InhaleStart = 0; // The time in each period where inhalation starts
double ExhaleStart = InhaleTimeRatio*BreathPeriod; // Time in each period where exhalation starts
double TB = 0;
int Mode = 0; // To track whether patient is inhaling or exhaling
int inhale = 0; // For mode tracking, inhale is 0, exhale is 1
int exhale = 1;

void setup() {
  Serial.begin(115200);
  
  InspiratoryValve.attach(InspiratoryValve_Pin_Output); // Attach servo to pin
}

void loop() {

  
  if (first)
  {
    for (int i = 0; i < offtset_avg; i++)
    {
        // Sensor 1
      if (iteration03 >= (avg_size))
      {
        iteration03 = 0; // Reset counter
      }
      Analog_in = analogRead(Pressure03_Pin_Input); // Read value from pressure sensor
      V_in = Analog_in*Analog_to_Voltage; // Convert to voltage
      V_in_Buffer03[iteration03] = V_in; // Log in buffer for averaging
      for (int i = 0; i < avg_size; i++) // Sum values over buffer
      {
        V_in_sum += V_in_Buffer03[i];
      }
      V_in = V_in_sum/avg_size; // Take average
      V_in_sum = 0; // Reset the sum
      Pressure_03PSI = V_in*25 - 39.19; // Equation for 0.3 PSI transducer ****** USE UPDATED EQUATION
      iteration03++; // Increment counter
    
      // Sensor 2
      if (iteration >= (avg_size))
      {
        iteration = 0; // Reset counter
      }
      Analog_in = analogRead(Pressure1_Pin_Input); // Read value from pressure sensor
      V_in = Analog_in*Analog_to_Voltage; // Convert to voltage
      V_in_Buffer[iteration] = V_in; // Log in buffer for averaging
      for (int i = 0; i < avg_size; i++) // Sum values over buffer
      {
        V_in_sum += V_in_Buffer[i];
      }
      V_in = V_in_sum/avg_size; // Take average
      V_in_sum = 0; // Reset the sum
      Pressure_1PSI = V_in*94.31 - 147.2; // Equation for 1.0 PSI transducer
      iteration++; // Increment counter
  
      Pressure03_total += Pressure_03PSI;
      Pressure1_total += Pressure_1PSI;
      
      delay(1);
    }
    first = 0;
    offset03 = Pressure03_total/offtset_avg;
    offset1 = Pressure1_total/offtset_avg;
    Serial.print(offset03);
    Serial.print(",");
    Serial.print(offset1);
    Serial.print(",");
  }
  
  // ***** Servo Control ***** //
  t = millis();
  T1 = (unsigned long int)(t / (TimeResolution)) % int(BreathPeriod / TimeResolution); // Converts realtime in CPU ("t") to the element index in the Reference signal. Time_Step multiplied to 1000 to convert it to micro second because "t" is in micro s.
  TB = T1*TimeResolution; // Actual time in (ms) during each breath and varies from 0 to Breath period.
  if (TB >=  InhaleStart && TB < ExhaleStart) // Patient is inhaling
  {
    Mode = inhale;
    InspiratoryValve.write(ValveOpen);
  }
  if (TB > ExhaleStart) // Patient is exhaling
  {
    Mode = exhale;
    InspiratoryValve.write(ValveClose);
  }

  // ***** Pressure Sensor Reading ***** //
  // Sensor 1
  if (iteration03 >= (avg_size))
  {
    iteration03 = 0; // Reset counter
  }
  Analog_in = analogRead(Pressure03_Pin_Input); // Read value from pressure sensor
  V_in = Analog_in*Analog_to_Voltage; // Convert to voltage
  V_in_Buffer03[iteration03] = V_in; // Log in buffer for averaging
  for (int i = 0; i < avg_size; i++) // Sum values over buffer
  {
    V_in_sum += V_in_Buffer03[i];
  }
  V_in = V_in_sum/avg_size; // Take average
  V_in_sum = 0; // Reset the sum
  Pressure_03PSI = V_in*26.01 - 45.92; // Equation for 0.3 PSI transducer ****** USE UPDATED EQUATION
  iteration03++; // Increment counter

  // Sensor 2
  if (iteration >= (avg_size))
  {
    iteration = 0; // Reset counter
  }
  Analog_in = analogRead(Pressure1_Pin_Input); // Read value from pressure sensor
  V_in = Analog_in*Analog_to_Voltage; // Convert to voltage
  V_in_Buffer[iteration] = V_in; // Log in buffer for averaging
  for (int i = 0; i < avg_size; i++) // Sum values over buffer
  {
    V_in_sum += V_in_Buffer[i];
  }
  V_in = V_in_sum/avg_size; // Take average
  V_in_sum = 0; // Reset the sum
  Pressure_1PSI = V_in*94.31 - 147.2; // Equation for 1.0 PSI transducer
  iteration++; // Increment counter

  // Check the current pressure is within allowance
  if (Mode == inhale)
  {
    if (Pressure_1PSI > pressure_inhale_max || Pressure_1PSI < pressure_inhale_min)
    {
      v_alarm = 1.0; // Set alarm
    }
  }
  else
  {
    if (Pressure_1PSI > pressure_exhale_max || Pressure_1PSI < pressure_exhale_min)
    {
      v_alarm = 1;
    }
  }

  Serial.println(Pressure_03PSI - offset03);
  Serial.print(",");
  Serial.println(Pressure_1PSI - offset1);

  delay(TimeResolution/avg_size); // To allow servo to achieve position

}
