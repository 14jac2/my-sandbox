# Ibex Project



## Overview

The "Ibex" project was a research project funded my the NSERC Canadian Robotics Network. The project involved myself and another student. We simulated mining equipment in ROS and Gazebo, and researched robotic platforms for autonomous mining systems. Ultimately, we designed and built a robotics platform over the course of approximately **two months** which enables dynamic levelling and clearance, named Ibex. 

For more information regarding the NSERC Canadian Robotics Network please visit [the NSERC webpage](https://ncrn-rcrc.mcgill.ca/) (also notice the Ibex is on the front page).

## Role

My role was to design the electrical control system, and program the control software. In particular, the robot emlployed eight motors to actuate the flipper treads, and networked via a Raspberry pie. An Xbox controller was used to actuate the flipper treads in various modes, over a bluetooth connection. The majority of the project was coded in Python and C++.

## Description

The Ibex was designed and built from the ground up. All parts were cut/manufactured either at Queen's University, or at our NSERC partner MacLean Engineering. The bearings, treads, sprockets, and electronics were outsourced. The project lasted approximately **two months**, and as such we were not afraid of overtime. The system was assembled in the lab over a series of a couple weeks as the parts arrived:

<img src="images/early_build.jpeg" alt="drawing1" width="250"/>

*Ibex being assembled.*


The control code was written in parallel to the assembly. A makeshift workstation was used to accees the electronics and assemble the build.

<img src="images/build.jpeg" alt="drawing2" width="250"/>
<img src="images/build_2.jpeg" alt="drawing3" width="350"/>

*Makeshift workstation.*

A lot of care was given to cable and wire routing to avoid snagging. Wires and cables were also labelled for easy assembly and troubleshooting.

<img src="images/first_build.jpg" alt="drawing4" width="300"/>

*Final assembly.*

After assembly, the platform was tested in the lab to ensure objectives were met. The system was indeed able to generate sufficient torque to adjust clearance and orientation.

<img src="images/standing.jpeg" alt="drawing5" width="250"/>

*Maximum clearance.*

Further upgrades were made to the system over years as new students employed the platform for research.

<img src="images/final.jpeg" alt="drawing6" width="250"/>

*Some upgrades by fellow researchers.*

The Ibex also managed to make appearances at the grand opening of the Mitchell Hall and Inginuity Labs.

<img src="images/on_stage.jpeg" alt="drawing7" width="250"/>

*Ibex making a public appearance.*

Please see the "videos/" folder for some footage of the Ibex performing clearance adjustments.


## Notes
If you are interested in robotics research please feel free to reach out to me at
[jack.caldwell@queensu.ca](mailto:jack.caldwell@queensu.ca).

Please visit [Offroad Robotics](https://offroad.engineering.queensu.ca/) for more similar research in the field of offroad robotics.

For more information regarding the NSERC Canadian Robotics Network please visit [the NSERC webpage](https://ncrn-rcrc.mcgill.ca/) (**also notice the Ibex is on the front page**).