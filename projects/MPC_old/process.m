function new_q = process(u, q, T, d, alpha, beta)
    new_q = alpha*q + T*beta*u + T*d;
end
