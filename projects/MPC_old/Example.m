%% Test code for GPR
% The following code attempts approaches to learning GP for testing

%% Define disturbance signal
simtime = 6;
T = 0.01;
N = simtime/T;
p = 10;
time = linspace(0,N,N);
alpha = 0.99;
beta = 0.5;
maxit = 100;
thresh = 0.001;
d = zeros(1, N);

for i = 1:(2*N/6)
    d(:,i) = 0;
end
for i = (2*N/6):(3*N/6)
    d(:,i) = 4;
end
for i = (3*N/6):(4.5*N/6)
    d(:,i) = 0;
end
for i = (4.5*N/6):(5.5*N/6)
    d(:,i) = 4;
end

%% Their GP Example
% The following recreates the example from the textbook for
% practice/varification
x = gpml_randn(0.8, 20, 1);                 % 20 training inputs
y = sin(3*x) + 0.1*gpml_randn(0.9, 20, 1);  % 20 noisy training targets
xs = linspace(-3, 3, 61)';                  % 61 test inputs 
  
meanfunc = [];                    % empty: don't use a mean function
covfunc = @covSEiso;              % Squared Exponental covariance function
likfunc = @likGauss;              % Gaussian likelihood

hyp = struct('mean', [], 'cov', [0 0], 'lik', -1);

hyp2 = minimize(hyp, @gp, -100, @infGaussLik, meanfunc, covfunc, likfunc, x, y);

[mu, s2] = gp(hyp2, @infGaussLik, meanfunc, covfunc, likfunc, x, y, xs);

f1 = [mu+2*sqrt(s2); flipdim(mu-2*sqrt(s2),1)];

figure(1);
fill([xs; flipdim(xs,1)], f1, [7 7 7]/8);
hold on; plot(xs, mu); plot(x, y, '+');
xlabel('Input, x');
ylabel('Output, y');


%% GP Train on disturbance
% We now try difference methods of training of the disturbance

x = time';
y = d';
xs = x;

meanfunc = [];                    % empty: don't use a mean function
covfunc = @covSEiso;              % Squared Exponental covariance function
likfunc = @likGauss;              % Gaussian likelihood

hyp = struct('mean', [], 'cov', [0 0], 'lik', -1);

hyp2 = minimize(hyp, @gp, -100, @infGaussLik, meanfunc, covfunc, likfunc, x, y);

[mu1, s2] = gp(hyp2, @infGaussLik, meanfunc, covfunc, likfunc, x, y, xs);

xaxis = linspace(0,6,N);

figure(2);
subplot(2,1,1);
plot(xaxis,d);
grid on;
ylim([-1 5]);
xlabel('Time (s)');
title('Actual disturbance signal');
subplot(2,1,2);
f = [mu1+2*sqrt(s2); flipdim(mu1-2*sqrt(s2),1)];
fill([xaxis'; flipdim(xaxis',1)], f, [7 7 7]/8);
hold on; 
plot(xaxis, mu1);
xlabel('Time (s)');
grid on;
title('GPR with 95% confidence bounds');
