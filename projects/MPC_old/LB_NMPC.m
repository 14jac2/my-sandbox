%% NMPC
simtime = 6;
T = 0.01;
N = simtime/T;
p = 10;
time = linspace(0,N,N);
alpha = 0.99;
beta = 0.5;
maxit = 100;
thresh = 0.001;
d = zeros(1, N+p);
small_Q = 10;
small_R = 0.01;
Q = eye(p)*small_Q;
R = eye(p)*small_R;

for i = 1:(2*N/6)
    d(:,i) = 0;
end
for i = (2*N/6):(3*N/6)
    d(:,i) = 4;
end
for i = (3*N/6):(4.5*N/6)
    d(:,i) = 0;
end
for i = (4.5*N/6):(5.5*N/6)
    d(:,i) = 4;
end

ud = zeros(1,N+p);
zd = zeros(1, N+p);
qd = zeros(1, N+p);

for i = 1:(N/6)
    zd(:,i) = 0;
    qd(:,i) = 0;
end
for i = (N/6):(5*N/6)
    zd(:,i) = 2;
    qd(:,i) = 2;
end
for i = (5*N/6):(N+p)
    zd(:,i) = 0;
    qd(:,i) = 0;
end

q_init = zeros(1);
q_init(1) = 0;

q_LBNMPC = zeros(1,N+p);
z_LBNMPC = zeros(1,N+p);
u_LBNMPC = zeros(1);
q_LBNMPC(:,1) = q_init;
u_LBNMPC(1) = 0;
z_LBNMPC(1,1) = q_init;

Hu = zeros(p,p);
Hz = zeros(p,p);

z_bar = zeros(1,p);
Zd = zeros(1,p);
u = zeros(1,p);

delta_u = zeros(1,p);

% Regression
% a = zeros(3, N+p);
a = zeros(1,N+p);
disturbance = zeros(1, N+p);

% Simulation Loop
for k = 2:N
    q_LBNMPC(:,k) = process(u_LBNMPC, q_LBNMPC(:,(k-1)), T, d(:,(k-1)), alpha, beta);
    z_LBNMPC(:,k) = q_LBNMPC(:,k);
    
    % Regresssion
    z_guess = nom_process(u_LBNMPC, q_LBNMPC(:, (k-1)), T, 0);
    disturbance(:,k) = z_LBNMPC(:,k) - z_guess;
%     a(:,k-1) = [z_LBNMPC(:,k-1), u_LBNMPC, k-1];
    a(:,k-1) = (k-1);
    
    Zd = zd(:,(k+1):(k+p));
    
    for i = 1:(p-1)
        u(:,i) = u(:,i+1);
    end
    u(:,p) = 0;
    
    for j = 1:maxit
        for b = 1:p
            if b < p
                Hz(b+1,b) = alpha;
            end
            Hu(b,b) = T*beta;
        end
        z_bar(:,1) = nom_process(u(:,1), z_LBNMPC(:,k), T, 0);
        for b = 2:p
            z_bar(:,b) = nom_process(u(:,b), z_bar(:,b-1), T, 0);
        end
        H_prime = (eye(p) - Hz)\Hu;
        z_tild = transpose(Zd - z_bar);
        delta_u = (transpose(H_prime)*Q*H_prime + R)\(transpose(H_prime)*Q*z_tild - R*transpose(u));
        flag = 0;
        u = u + transpose(delta_u);
        for b = 1:p
            if delta_u(b,:) < thresh && delta_u(b,:) > -thresh
                flag = flag + 1;
            end
        end
        if flag == p
            break
        end
    end
    u_LBNMPC(:) = u(1,1);
    
end

%% Plotting
% Plot the results of the controller
figure(1)
subplot(3,1,1)
plot(time, qd(:,1:N), 'r');
hold on
plot(time, q_LBNMPC(:,1:N), 'b');
hold off
subplot(3,1,2)
plot(time, qd(:,1:N) - q_LBNMPC(:,1:N), 'r');
subplot(3,1,3)
plot(time, d(:, 1:N));

%% GP Train

x = a';
y = disturbance';
xs = x;

meanfunc = [];                    % empty: don't use a mean function
covfunc = @covSEiso;              % Squared Exponental covariance function
likfunc = @likGauss;              % Gaussian likelihood

hyp = struct('mean', [], 'cov', [0 0], 'lik', -1);

hyp2 = minimize(hyp, @gp, -100, @infGaussLik, meanfunc, covfunc, likfunc, x, y);

[mu1, s2] = gp(hyp2, @infGaussLik, meanfunc, covfunc, likfunc, x, y, xs);

test = linspace(0,6,N);

%% Plot GPR
% Plot learnt and actual disturbance
figure(2);
subplot(2,1,1);
plot(test,disturbance(1,1:N));
grid on
ylim([-0.2 0.2]);
xlabel('Time (s)');
title('Measured disturbance signal');
subplot(2,1,2);
f = [mu1(1:N-1,1)+2*sqrt(s2(1:N-1,1)); flipdim(mu1(1:N-1,1)-2*sqrt(s2(1:N-1,1)),1)];
check = [xs(1:N-1,1); flipdim(xs(1:N-1,1),1)];
fill(check, f, [7 7 7]/8);
hold on; 
plot(time(1:N-1), mu1(1:N-1,1));
hold off;
grid on;
ylim([-0.2 0.2]);
xlabel('Time (s)');
title('Predicted disturbance signal');

%% Simulation Iteration 2
% The following was an attempt at a second trial such that the prediction
% of the GP could be improved. The code does not work currently.

% q_LBNMPC = zeros(1,N+p);
% z_LBNMPC = zeros(1,N+p);
% u_LBNMPC = zeros(1);
% q_LBNMPC(:,1) = q_init;
% u_LBNMPC(1) = 0;
% z_LBNMPC(1,1) = q_init;
% 
% Hu = zeros(p,p);
% Hz = zeros(p,p);
% 
% z_bar = zeros(1,p);
% Zd = zeros(1,p);
% u = zeros(1,p);
% predicted_disturbances = zeros(1,N);
% 
% delta_u = zeros(1,p);
% 
% % Regression
% pred_disturbance = zeros(p, 1);
% pred_inputs = zeros(3,p);
% 
% % Simulation Loop
% for k = 2:N
%     q_LBNMPC(:,k) = process(u_LBNMPC, q_LBNMPC(:,(k-1)), T, d(:,(k-1)), alpha, beta);
%     z_LBNMPC(:,k) = q_LBNMPC(:,k);
%     
%     Zd = zd(:,(k+1):(k+p));
%     
%     for i = 1:(p-1)
%         u(:,i) = u(:,i+1);
%     end
%     u(:,p) = 0;
%     
%     for j = 1:maxit
%         for b = 1:p
%             if b < p
%                 Hz(b+1,b) = alpha;
%             end
%             Hu(b,b) = T*beta;
%         end
%         pred_inputs = [z_LBNMPC(:,k); u(:,1); k];
%         [pred_disturbance, s2] = gp(hyp2, @infGaussLik, meanfunc, covfunc, likfunc, x, y, pred_inputs);
%         predicted_disturbances(:,k) = pred_disturbance(1);
%         z_bar(:,1) = nom_process(u(:,1), z_LBNMPC(:,k), T, pred_disturbance(1));
%         for b = 2:p
%             pred_inputs = [z_LBNMPC(:, k+b-1), u(:,b), k+b-1];
%             [pred_disturbance, s2] = gp(hyp2, @infGaussLik, meanfunc, covfunc, likfunc, x, y, pred_inputs');
%             z_bar(:,b) = nom_process(u(:,b), z_bar(:,b-1), T, pred_disturbance(1));
%         end
%         H_prime = (eye(p) - Hz)\Hu;
%         z_tild = transpose(Zd - z_bar);
%         delta_u = (transpose(H_prime)*Q*H_prime + R)\(transpose(H_prime)*Q*z_tild - R*transpose(u));
%         flag = 0;
%         u = u + transpose(delta_u);
%         for b = 1:p
%             if delta_u(b,:) < thresh && delta_u(b,:) > -thresh
%                 flag = flag + 1;
%             end
%         end
%         if flag == p
%             break
%         end
%     end
%     u_LBNMPC(:) = u(1,1);
%     
% end
% 
% figure(3)
% subplot(3,1,1)
% plot(time, qd(:,1:N), 'r');
% hold on
% plot(time, q_LBNMPC(:,1:N), 'b');
% hold off
% subplot(3,1,2)
% plot(time, qd(:,1:N) - q_LBNMPC(:,1:N), 'r');
% subplot(3,1,3)
% plot(time, d(:, 1:N));
% 
% 
% %% Plot 
% 
% figure(6)
% plot(time, predicted_disturbances);