# Anti-Social Tree



## About

This was a fun side project for a casual competition at my graduate lab. The prupose of the competition was to design and build a Christmas themed robotic system. Our team designed and built a mobile tree platform which signs when alone, but is silent when around people and moves away from crowds. See a sample video in the "videos" folder. We won the competition.

<img src="images/Plaque.jpg" alt="drawing1" width="250"/>

*Plaque for winning lab competition.*

