
#include <iostream>
#include <vector>

using namespace std;

//Edge class for graph
class Edge {
    public:
        int src, dest;
        Edge(int src, int dest) {
            this->src = src;
            this->dest = dest;
        }
};

//Graph class
class Graph {
    public:
        vector<vector<int> > adjList;
        Graph(vector<Edge> const &edges, int N) {
            adjList.resize(N);
            for(auto &edge: edges) {
                adjList[edge.src].push_back(edge.dest);
                adjList[edge.dest].push_back(edge.src);
            }
        }
};

//Main function
int main() {
    //Array of edges in graph
    vector<Edge> edges = {
        {0, 1}, {1, 2}, {2, 0}, {2, 1},
        {3, 2}, {4, 5}, {5, 4}
    };
    //Number of nodes in
    int N = 6;
    //Create a graph from given edges
    Graph graph(edges, N);
    //Print adjacency list representation of graph
    for (int i = 0; i < N; i++) {
        cout << i << " --> ";
        for (int v : graph.adjList[i])
            cout << v << " ";
        cout << endl;
    }
    return 0;
}