# My Projects



## Preface

This repo shall house test code old and new robotics projects. A brief description is provided herein of each of the projects. Projects of note: Ibex, Ventillator, and Masters.

### Masters

My masters work comprised of the conceptualization, design, and implementation of manipulator controllers. In particular, I designed a novel learning-based model predictive controller. Furthermore, I tested a number of the controllers on a hydraulic loader. The repo herein is only for videos/links, as the code belongs to my benefactors.

### Ibex

This was a summer project during my undergrad wherein myself and another student designed and built a mobile base with autonomous mining in mind. The robot is able to dynamically change clearance and orientation for self-levelling.

### Ventillator

I particupated in the Code Life Ventillator challenge at the beggining of the COVID-19 pandemic. The purpose was to design and build a cheap and easy to manufacture ventillator for developping countries.

### Playpen

This directory will house all test code to try and learn new technologies. In particular, I am currently exploring designing C++ structures for graphs.

### Glove

An undergraduate project involved the design and assembly of a robotic system. Myself and another student designed a sensory glove which controlled a 3D printed hand.

### Anti-social Tree
The anti-social tree was a fun little side project created for a small lab competition at Inginuity Labs. The tree is mounted on a mobile base, has some flashing lights, and plays music. Using ultrasonic sensors, when a human approaches the tree would darken the lights, stop playing music, and drive away from the human.

### MPC_old

This is a dump of some old code used to test ideas for NMPC and GP-MPC in Matlab. Note, this code is not vetter, and was not the code used as my masters work. The code in this repo was pre-test before the controllers were implemented in Python.

### ELEC 848

This repo houses some coursework from the Queen's University graduate course "Control System Design for Robots and Telerobots".

### ELEC 825

This repo houses some coursework from the Queen's University graduate course "Autonomous Vehicle Control and Navigation".

### ELEC 474

This repo houses some coursework from the Queen's University course "Machine Vision".