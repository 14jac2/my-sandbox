% Run this script to test all controllers.

%% Simulating

% Different controllers
inverse = true;
robust = false; % Inverse dynamics with robuts (and sliding mode)
soft = false; % Inverse dynamics with robust and hard switch (and sliding mode)
SMSoft = false; % Sliding mode with soft switch
SL = false; % Slotine and Lie controller

% Actual dynamics
I = 10; 
MgL = 10; 

% Estimated dynamics
I_hat = 5; 
MgL_hat = 5; 

% Controller
wn = 3;
zeta = 1;

model = 'Caldwell_Assignment3_Sim';
t_max = 25;
dt_max = 0.01;

K0 = wn^2;
K1 = 2*zeta*wn;
A_bar = [0,1;-K0,-K1];
P = lyap(transpose(A_bar),eye(2));

% The simulation will run for each configuration
% Controller 1: Iverse Dynamics
simdata1 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% Controller 2: Robust Inverse Dynamics
robust = true;
simdata2 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));
 
% Controller 3: Robust Inverse Dynamics with soft switch
soft = true;
simdata3 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% % Controller 4: Sliding mode with hard
inverse = false;
simdata4 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% % Controller 5: Sliding mode with soft
SMSoft = true;
simdata5 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));
% 
% % % Controller 6: Slotine and Li
% SL = true;
% simdata6 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

%% Plotting
% Inverse Dynamics
figure (1)
subplot(3,1,1);
plot(simdata1.q.time(:), simdata1.q.signals(2).values(:, 1), 'r');
hold on
plot(simdata1.q.time(:), simdata1.q.signals(1).values(:, 1), 'b');
hold off
xlabel('Time (s)');
ylabel('q (rad)');
grid on
legend('Desired', 'Actual');
title('Joint Angle');

subplot(3,1,2);
plot(simdata1.e(:,1), simdata1.e(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Error (rad)');
title('Joint Angle Error');

subplot(3,1,3);
plot(simdata1.Control(:,1), simdata1.Control(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Control (Nm)');
title('Joint Control Input');
sgtitle('Inverse Dynamics Controller');

% Robust Inverse Dynamics
figure (2)
subplot(4,1,1);
plot(simdata2.q.time(:), simdata2.q.signals(2).values(:, 1), 'r');
hold on
plot(simdata2.q.time(:), simdata2.q.signals(1).values(:, 1), 'b');
hold off
grid on
xlabel('Time (s)');
ylabel('q (rad)');
legend('Desired', 'Actual');
title('Joint Angle');

subplot(4,1,2);
plot(simdata2.e(:,1), simdata2.e(:,2), 'b');
grid on
xlabel('Time (s)');
ylabel('Error (rad)');
title('Joint Angle Error');

subplot(4,1,3);
plot(simdata2.Control(:,1), simdata2.Control(:,2), 'b');
grid on
xlabel('Time (s)');
ylabel('Control (Nm)');
title('Joint Control Input');

[E1,Ed1] = meshgrid(simdata2.e(:,2), simdata2.e_dot(:,2));
for i=1:length(simdata2.e(:,1))
    for j=1:length(simdata2.e(:,1))
      V1(i,j) = [E1(i,j),Ed1(i,j)] * P * [E1(i,j);Ed1(i,j)] ;
    end
end
subplot(4,1,4);
mesh(E1, Ed1, V1);
title('Lyapunov Function');

sgtitle('Robust Inverse Dynamics Controller');

% Robust Inverse Dynamics - Soft switch
figure (3)
subplot(4,1,1);
plot(simdata3.q.time(:), simdata3.q.signals(2).values(:, 1), 'r');
hold on
plot(simdata3.q.time(:), simdata3.q.signals(1).values(:, 1), 'b');
hold off
grid on
xlabel('Time (s)');
ylabel('q (rad)');
legend('Desired', 'Actual');
title('Joint Angle');

subplot(4,1,2);
plot(simdata3.e(:,1), simdata3.e(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Error (rad)');
title('Joint Angle Error');

subplot(4,1,3);
plot(simdata3.Control(:,1), simdata3.Control(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Control (Nm)');
title('Joint Control Input');

[E2,Ed2] = meshgrid(simdata3.e(:,2), simdata3.e_dot(:,2));
for i=1:length(simdata3.e(:,1))
    for j=1:length(simdata3.e(:,1))
      V2(i,j) = [E2(i,j),Ed2(i,j)] * P * [E2(i,j);Ed2(i,j)] ;
    end
end
subplot(4,1,4);
mesh(E2, Ed2, V2);
title('Lyapunov Function');

sgtitle('Sobust Inverse Dynamics Controller with Soft Switch');

% Sliding Mode
figure (4)
subplot(4,1,1);
plot(simdata4.q.time(:), simdata4.q.signals(2).values(:, 1), 'r');
hold on
plot(simdata4.q.time(:), simdata4.q.signals(1).values(:, 1), 'b');
hold off
grid on
xlabel('Time (s)');
ylabel('q (rad)');
legend('Desired', 'Actual');
title('Joint Angle');

subplot(4,1,2);
plot(simdata4.e(:,1), simdata4.e(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Error (rad)');
title('Joint Angle Error');

subplot(4,1,3);
plot(simdata4.Control(:,1), simdata4.Control(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Control (Nm)');
title('Joint Control Input');
sgtitle('Sliding Mode Controller');

subplot(4,1,4);
plot(simdata4.q_dot(:,2), simdata4.e(:,2) , 'b');
grid on
xlabel('q_dot (rad/s)');
ylabel('Error (rad)');
title('Phase Plane');
sgtitle('Sliding Mode Controller');

% Sliding Mode - soft
figure (5)
subplot(4,1,1);
plot(simdata5.q.time(:), simdata5.q.signals(2).values(:, 1), 'r');
hold on
plot(simdata5.q.time(:), simdata5.q.signals(1).values(:, 1), 'b');
hold off
grid on
xlabel('Time (s)');
ylabel('q (rad)');
legend('Desired', 'Actual');
title('Joint Angle');

subplot(4,1,2);
plot(simdata5.e(:,1), simdata5.e(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Error (rad)');
title('Joint Angle Error');

subplot(4,1,3);
plot(simdata5.Control(:,1), simdata5.Control(:,2) , 'b');
grid on
xlabel('Time (s)');
ylabel('Control (Nm)');
title('Joint Control Input');

subplot(4,1,4);
plot(simdata5.q_dot(:,2), simdata5.e(:,2) , 'b');
grid on
xlabel('q_dot (rad/s)');
ylabel('Error (rad)');
title('Phase Plane');

sgtitle('Sliding Mode Controller with Soft Switch');
% 
% % Slotine and Lie
% figure (6)
% subplot(3,1,1);
% plot(simdata6.q.time(:), simdata6.q.signals(2).values(:, 1), 'r');
% hold on
% plot(simdata6.q.time(:), simdata6.q.signals(1).values(:, 1), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint Angle');
% 
% subplot(3,1,2);
% plot(simdata6.e(:,1), simdata6.e(:,2) , 'b');
% grid on
% title('Joint Angle Error');
% 
% subplot(3,1,3);
% plot(simdata6.Control(:,1), simdata6.Control(:,2) , 'b');
% grid on
% title('Joint Control Input');
% sgtitle('Slotine and Lie Controller');
