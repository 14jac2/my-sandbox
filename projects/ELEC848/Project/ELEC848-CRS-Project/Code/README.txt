README for ELEC 848 Project
CRS A465 robot

This folder contains the code and data required to replicate the results of Caldwell, Huckell, Mansourian, and Sears (2020). This file explains how to use the tools to model the CRS A465 robot.

----------
Subfolders
----------

Dynamics and Kinematics
->dynamicParameterEst
    + Run this function to run a least square regression estimating the dynamic parameters for a given dynamic model
->dynamicParameterVerify
   + Run this function to predict the dynamics of the dynamic model given an estimated dynamic parameter
->Dynamics
   + Run this funciton first to gernerate the symbolic dynamic model (Y, theta)  for the CRS manipulator of the form Tau = Y*Theta where Tau is the joint torques, Y is the regressor and Theta are the dynamic parameters
->Dynamics_withFric 
   + Run this function instead of Dynamics to obtain a dynamic model that includs coloumb and viscous friction at each joint.
->Homogenous_transforms 
   + This is a function used within the Lagrange function. It generates the homogenous transformation matrices given DH parameters
->Jacobian 
   + This function is used within both the Dynamics and Force_Estimation to generate the Jacobian for the differential kinematic model of the CRS manipulator
-> Lagrange 
   + This function is used within both Dynamics and Dynamics_withFric. It is used to symbolically calcualte the Euler Lagrange equation for the CRS manipulator
-> LagrangeTimeDerivative 
   + This function is used within Lagrange. This function computes the symbolic expression of d/dt(del L/ del q_dot)
-> Model_Reduction.m
    + This function is used within dynamic ParameterEst to reduce theta parameters based on linear dependencies between stacked Y matrix columns


Force
-> Force_Regression.m
    + Run this function after dynamics regression to determine (regress) the motor constant given joint positions, voltages, and generlized force information.
-> Force_Estimation_2019.m
    + Run this function after the dynamics and force regression to estimate end effector forces as a function of joint position and voltage information presented in the 2019 format.
-> Force_Estimation_2020.m
    + Run this function after the dynamics and force regression to estimate end effector forces as a function of joint position and voltage information presented in the 2020 format.

Testing and Data (CRS A465)
=> Raw Data
    + Data as recorded by the robot controller
=> Trajectories
    + Folder containing input trajectories for robot controller
-> Generate_Joint_Trajectories.m
    + This function will generate multi-joint trajectories to be used when collecting data for dynamic characterization of the CRS A465 robot.
-> Parse_Robot_Data.m
    + Run this function to convert raw data to parsed data. Removes the unused data from the raw data and parses the usable data from the time series joint position, velocities, and accelerations.