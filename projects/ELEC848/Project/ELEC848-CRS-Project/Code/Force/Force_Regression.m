function K = Force_Regression(dataPath,Y_func, T, idx, Theta_BIG, Jac)
%% Acquire Y and Fext
% Source file for regression
Path = strcat(dataPath,'/test4-ww_parsed.mat');

[file,path] = uigetfile(Path,'SELECT PARSED DATA: 4-5');
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

load(filename);

% Smooth data
new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;
new_voltage = smoothdata(voltage, 'gaussian', 100);
new_force = smoothdata(force, 'gaussian', 100);

% Build our Y and Fext matrices
first = 1;
last = length(voltage);
num_params = length(T);
first = first + round(last*0.2);
last = first + 1000;

Y_BIG = zeros(3*(last-first+1),num_params);
Fext = zeros(6*(last-first+1),1);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
    Fext(row:row+5) = new_force(i,1:6); % Assuming first three correspond with forces not torques
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);

% Perform prediction on dynamics related voltage
dynamics = L_BIG * Theta_BIG;

%% Acquire other equation parameters
% Define Jacobian function for forces
J_func = matlabFunction(Jac);

V_BIG = zeros(3*(last-first+1),1);
TAU_EXT_BIG = zeros(3*(last-first+1),1);

% Populate matrices
for i = first : last
    row = 3*(i-first) + 1;
    J = J_func(new_q(i,1), new_q(i,2), new_q(i,3));
    Voltage = new_voltage(i,1:3).';
    
    gamma = new_force(i,1:6);
    TAU_EXT_BIG(row:row+2) = (transpose(J)*transpose(gamma));
    V_BIG(row:row+2) = (Voltage - dynamics(row:row+2));
end

%% Regression
% Perform regression
K = ((V_BIG.' * V_BIG) \ V_BIG.')*TAU_EXT_BIG;

TAU_BIG = reshape(TAU_BIG,3,[]).';
dynamics = reshape(dynamics,3,[]).';

% Plot actual and predicted voltages (the difference is what is used for
% regression)
figure
subplot(3,1,1)
plot(TAU_BIG(:,1));
hold on
plot(dynamics(:,1));
hold off
ylabel('Voltage (V)');
legend('Actual','Predicted Dynamics');
title('Joint 1');
subplot(3,1,2)
plot(TAU_BIG(:,2));
hold on
plot(dynamics(:,2));
hold off
ylabel('Voltage (V)');
title('Joint 2');
subplot(3,1,3)
plot(TAU_BIG(:,3));
hold on
plot(dynamics(:,3));
hold off
ylabel('Voltage (V)');
xlabel('Time');
title('Joint 3');
sgtitle('Difference in Voltage');

end