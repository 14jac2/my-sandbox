%% ELEC 848 CRS Dynamics
% Attempted by Jack Caldwell on April 3, 2020

function [PE, KE] = Lagrange()
%% Define parameters
syms m1 m2 m3 lc1 lc2 lc3 I1xx I1yy I1zz I2xx I2yy I2zz I3xx I3yy I3zz;          
syms q1 q2 q3;
syms Dq1 Dq2 Dq3;
syms DDq1 DDq2 DDq3;
% syms g ;   % gravity

% Principal inertias
I1 = diag([I1xx I1yy I1zz]);
I2 = diag([I2xx I2yy I2zz]);
I3 = diag([I3xx I3yy I3zz]);

% Centers of mass
Lc1 = [0 -lc1 0];
Lc2 = [lc2 0 0];
Lc3 = [lc3 0 0];

q   = [q1 q2 q3];
Dq  = [Dq1 Dq2 Dq3];
DDq = [DDq1 DDq2 DDq3];

G = [0;0;9.81];

% DH parameters
d_i = [0 0 0];
a_i = [0 305 330+76]*1e-3;
alpha_i = [pi/2 0 0];

%% Find transforms
% Generate symbolic functions of the homogenous transformation and Jacobian
[T01,T02,T03,T0ee] = Homogenous_transforms(d_i,alpha_i,a_i);

syms A01 A02 A03

A01 = T01(q1);
A02 = T02(q1,q2);
A03 = T03(q1,q2,q3);

R01 = A01(1:3,1:3);
R02 = A02(1:3,1:3);
R03 = A03(1:3,1:3);

Jac = Jacobian;

%% Find Lagrange parameters
% r0i-1,ci = New centers of mass wrt frame i-1 (expressed in frame 0)
r00c1 = R01*transpose(Lc1);
r01c2 = R02*transpose(Lc2);
r02c3 = R03*transpose(Lc3);

Omg01 = Jac(4:6,1)*Dq1;
Omg02 = Omg01 + Jac(4:6,2)*Dq2;
Omg03 = Omg02 + Jac(4:6,3)*Dq3;

Omg11 = transpose(R01)*Omg01;
Omg22 = transpose(R02)*Omg02;
Omg33 = transpose(R03)*Omg03;

P01 = A01(1:3,4);
P02 = A02(1:3,4);

Pc1 = r00c1;
Pc2 = P01 + r01c2;
Pc3 = P02 + r02c3;

vc1 = cross(Omg01,r00c1);
vc2 = P01 + cross(Omg02,r01c2);
vc3 = P02 + cross(Omg03,r02c3);

%% Find Kinetic, KEi = 1/2*(mi*vci*vci + wii*Ii*wii)
KE1 = (m1*transpose(vc1)*vc1 + transpose(Omg11)*I1*Omg11)/2;
KE2 = (m2*transpose(vc2)*vc2 + transpose(Omg22)*I2*Omg22)/2;
KE3 = (m3*transpose(vc3)*vc3 + transpose(Omg33)*I3*Omg33)/2;
KE = (KE1 + KE2 + KE3);

%% Find Potential, PEi = -mi*g*Pci
PE1 = -m1*transpose(G)*Pc1;
PE2 = -m2*transpose(G)*Pc2;
PE3 = -m3*transpose(G)*Pc3;
PE = (PE1 + PE2 + PE3);

%% Find Lagrange
L = KE - PE;

%% Find Mass matrix (factor joint velocities)

%% Find Gravity matrix

%% Find Coriolis

%% Find Friction

%% Return

end