function [RMSerror, predicted_voltage] = dynamicParameterVerify(dataPath,T,Y_func,idx,ThetaEst)
% dynamicParameterVarify computes the RMS error on the predicted dynamics
% using T_est = L*Theta_est where Theta_est is the estimated dynamic
% parameters from the dynamicParameterVarify function which computed the
% least squares regression to estimate Theta. The function plots both the 
% actual and predicted dynamics. 
% The function takes as inputs:
% dataPath: folder path to CRS data
% T: symbolic expresion of dynmaic parameters
% Y_func: symbolic funciton of the Y matrix T=Y*Theta
% idx: are the linearly independent columns of Y determined from the model
% reduction to obtain the reduced L
% ThetaEst: is the numerical estimate of the dynamic parameters

%% Select Data
% Select one of the other two free motion data sets no selected in part 1
Path = strcat(dataPath,'/test3_parsed.mat');

[file,path] = uigetfile(Path,'SELECT DIFFERENT PARSED DATA THEN IN FIRST STEP');
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

load(filename);

%% Filter Data

new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;

new_voltage = smoothdata(voltage, 'gaussian', 100);

%% Set range of data to analyze

first = 1;
last = length(voltage);
num_params = length(T);
first = first + round(last*0.2);
last = first + 4000;

%% Construct Stacked Y and Tau 

Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);


% Perform prediction
predicted_voltage = L_BIG * ThetaEst;

%% Plot

TAU_BIG = reshape(TAU_BIG,3,[]).';
predicted_voltage = reshape(predicted_voltage,3,[]).';

subplot(3,1,1)
plot(time(first:last)-time(first),TAU_BIG(:,1));
hold on
plot(time(first:last)-time(first),predicted_voltage(:,1));
hold off
legend('V_1 actual','V_1 predicted')
ylabel('voltage [V]')
subplot(3,1,2)
plot(time(first:last)-time(first),TAU_BIG(:,2));
hold on
plot(time(first:last)-time(first),predicted_voltage(:,2));
hold off
legend('V_2 actual','V_2 predicted')
ylabel('voltage [V]')
subplot(3,1,3)
plot(time(first:last)-time(first),TAU_BIG(:,3));
hold on
plot(time(first:last)-time(first),predicted_voltage(:,3));
hold off
legend('V_3 actual','V_3 predicted')
xlabel('time [s]')
ylabel('voltage [V]')

%% determine the RMS error as percentage of the voltage amplitude.

V_amp = mean( [max(new_voltage(:,1:3)) ; abs(min(new_voltage(:,1:3)))]);

RMSerror = sqrt( sum( ((TAU_BIG - predicted_voltage)./V_amp ).^2 )/length(TAU_BIG(:,1)));