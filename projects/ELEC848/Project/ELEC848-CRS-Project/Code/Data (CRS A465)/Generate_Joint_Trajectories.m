%% Create sinusoidal trajetory for CRS A465 manipulator (joint space)

% Set which joints you want to move
DOF = 1:3;
DOF_available = 6;

% Set the trajectory timing
runtime = 30; % [s]
timestep = 0.1; % [s]
steps = runtime/timestep;
time_offset = 20; % [s] as dictated by robot operations
time = 0:timestep:runtime-timestep;

% Manipulator limits
limits = [175, -175; % [degrees]
           90,  -90;
          110, -110; % -20 to 200
          180, -180;
          105, -105;
          180, -180];
limits = limits * (pi/180); % [rad] conversion
link_offset = [0, 0, -pi/2, 0, 0, 0]; % [rad] the starting position of the arm
      
% Create sinusoids
Q = zeros(steps,DOF_available);
Q = Q + repmat(mean(limits,2)',steps,1);

for ii = DOF % Each joint
    link_range = limits(ii,:);
    
    % Want a set of sinusoids that cover MOST of the range
    N = 6; % number of sinusoids
    link_diff = link_range(1)-link_range(2);
    amp = 0.25; % first frequency amplitude ratio of max range
    freq = (2*pi)/runtime; % first frequency is full period
    
    for jj = 1:N % Each sinusoid
        this_amp = amp/(2^(jj-1)) * link_diff;
        this_freq = freq*(1.3^(jj-1));        
        
        for kk = 1:steps % Each timestep
            % add sine signal to existing point
            Q(kk,ii) = Q(kk,ii) + this_amp * sin(this_freq*(kk-1) + link_offset(ii));
            
        end
        
    end
    %
end

figure; 
for ii = 1:DOF_available
    subplot(DOF_available,1,ii)
    plot(time,Q(:,ii));
end

setuptime = 1:20;

samplingtime = 20 + time;
test_time = [setuptime samplingtime]';

joint_start = repmat(link_offset,20,1);
joints = Q;
joint_trajectory = (180/pi)*[joint_start; joints];

test_trajectory = [test_time joint_trajectory];