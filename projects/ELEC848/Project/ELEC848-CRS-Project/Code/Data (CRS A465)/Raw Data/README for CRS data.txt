README for CRS data
Written by Thomas Sears and Jack Caldwell, April 2020.
thomas.sears@queensu.ca, jack.caldwell@queensu.ca

2019_jointangles.mat    Joint position information for 2019 1 kg test (6 DOF)
2019_voltages.mat       Joint voltage information for 2019 1 kg test (6 DOF)
test1.mat               Raw file from a free-space test on the CRS manipulator (3 DOF only)
test1_parsed.mat        Generated from test1.mat, removed pre- and post- test data from the raw files and removed extraneous variables
test2.mat               Raw file from a free-space test on the CRS manipulator (3 DOF only)
test2_parsed.mat        Generated from test2.mat, removed pre- and post- test data from the raw files and removed extraneous variables
test3.mat               Raw file from a free-space test on the CRS manipulator (3 DOF only)
test3_parsed.mat        Generated from test3.mat, removed pre- and post- test data from the raw files and removed extraneous variables
test4-ww.mat            Raw file from a weighted (100 g) test on the CRS manipulator (3 DOF only)
test4-ww_parsed.mat     Generated from test4.mat, removed pre- and post- test data from the raw files and removed extraneous variables
test5-ww.mat            Raw file from a weighted (100 g) test on the CRS manipulator (3 DOF only)
test5-ww_parsed.mat     Generated from test5.mat, removed pre- and post- test data from the raw files and removed extraneous variables

Contents of "2019_*.mat" files are as follows:
VARIABLE NAME       SIZE        CONTENTS
Voltages            N x 7       Column 1 = time, Columns 2-7 = joint voltages
Joint_angles        N x 7       Column 1 = time, Columns 2-7 = joint positions

Contents of "*_parsed.mat" files are as follows:
VARIABLE NAME       SIZE        CONTENTS
force               N x 6       Generalized force data from the end effector.
q                   N x 6       Angle positions of the manipulator joints. Degrees. Columns 1 to 6 are joints 1 to 6. NOTE: Joint 4,5,6 are not actuated in any tests.
q_dot               N x 6       Angular velocity of the manipulator joints. Degrees per second. Columns 1 to 6 are joints 1 to 6.
q_ddot              N x 6       Angular acceleration of the manipulator joints. Degrees per second squared. Columns 1 to 6 are joints 1 to 6. 
time                N x 1       Time vector of the test.
voltage             N x 6       Joint voltages (analogous to torque) for each of the manipulator motors. Volts. Columns 1 to 6 are joints 1 to 6.

Contents of raw files are not uniform. If you want to know about those, please consult with the robot operators as I do not know all of them!