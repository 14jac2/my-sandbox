%% Script to open and parse CRS A465 data
% This script accepts raw data (e.g. "test1.mat") and outputs a parsed
% version (e.g. "test1_parsed.mat"). During the parsing, plots are
% generated so the user can verify the data was collected properly.
clearvars

% Get file from user input
[file,path] = uigetfile('*.mat');
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

% Open .mat data file and check for the important datasets
load(filename);
list = {'q','q_dot','q_ddot','Voltages','ForceAndMoments'};
escape = false;

for ii = 1:size(list,2)
    A = exist(list{ii},'var');
    if ~A
        fprintf('%15s\t: missing\n', list{ii})
        escape = true;
    else
        fprintf('%15s\t: found\t\tsize: [%d x %d]\n', list{ii}, size(eval(list{ii})))
    end
end
if escape
    return
end

% get matching trajectory info
fid = fopen(fullfile(path,'data-trajectory-pairings.txt'));
pairings = textscan(fid,'%s %s');
fclose(fid);

trajectory_file = pairings{2}{contains(pairings{1},file)};
load(fullfile(path,trajectory_file));

%--------------------
% Extract useful data
%--------------------
new_list={'time','q','q_dot','q_ddot','voltage','force'};

% reassign data to new variable names
q = eval(list{1});
q_dot = eval(list{2});
q_ddot = eval(list{3});
voltage = eval(list{4});

% grab forces for weighted tests
force = eval(list{5});

% extract time from any of the datasets
time = q(:,1); % [s] at 500 Hz

% remove redundant time column
q(:,1) = [];
q_dot(:,1) = [];
q_ddot(:,1) = [];
voltage(:,1) = [];
force(:,1) = [];

% --------------------------
% Extract valid test results
% --------------------------
% Know that all tests start at 20 seconds, but when do they end? Check the
% "test_trajectory" file we sent
% t_start = 20;
% t_end = test_trajectory(end,1);
t_start = 10;
t_end = 15;

row_start = find(time==t_start);
row_end = find(time==t_end);

time = time(row_start:row_end,:);
q = q(row_start:row_end,:);
q_dot = q_dot(row_start:row_end,:);
q_ddot = q_ddot(row_start:row_end,:);
voltage = voltage(row_start:row_end,:);
force = force(row_start:row_end,:);

% -------------
% Plot the data
% -------------
figure('Position',[692 578 803 571]);
subplot(4,1,1)
plot(time,q)
title('Joint angle')
ylabel('Position [deg]')
ylim([-180, 180])
yticks([-180 -90 0 90 180])

subplot(4,1,2)
plot(time,q_dot)
title('Joint velocity')
ylabel('Velocity [deg/s]')

subplot(4,1,3)
plot(time,q_ddot)
title('Joint acceleration')
ylabel('Acceleration [deg/s^2]')

subplot(4,1,4)
plot(time,voltage)
title('Joint voltage')
ylabel('Voltage [V]')

xlabel('Time [s]')

% ------------------
% Clean up workspace
% ------------------
% new_filename = fullfile(path,[file(1:end-4) '_parsed.mat']);
% clearvars('-except',new_list{:},'new_filename')
% save(new_filename)

[~,newfile,~] = fileparts(file);
print(gcf,newfile,'-dpng','-r600');