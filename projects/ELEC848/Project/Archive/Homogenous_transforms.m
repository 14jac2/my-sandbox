% homogeneous transform
function [A01,A02,A03,A0ee] = Homogenous_transforms(d_i,al_i,a_i)

syms A(d,th,a,al)
A(d,th,a,al) = [cos(th), -sin(th)*cos(al),  sin(th)*sin(al), a*cos(th);
                sin(th),  cos(th)*cos(al), -cos(th)*sin(al), a*sin(th);
                      0,          sin(al),          cos(al),         d;
                      0,                0,                0,         1];

% dh parameters
% variables:
syms q1 q2 q3 
q_i = [q1, q2 + pi/2 ,q3 ];



% generate all the transforms
syms A1(q1) A2(q2) A3(q3)   
syms A01(q1) A02(q1,q2) A03(q1,q2,q3) A0ee(q1,q2,q3) 

A1(q1) = A(d_i(1), q_i(1), a_i(1), al_i(1));
A2(q2) = A(d_i(2), q_i(2), a_i(2), al_i(2));
A3(q3) = A(d_i(3), q_i(3), a_i(3), al_i(3));
Aee    = [ 0 0 1 0; 0 -1 0 0; 1 0 0 0; 0 0 0 1];

% build the full forward transform -- this is the kinematic model
% to get A_full(q_i), use result = subs(A_full, q, q_i)
% use q = symvar(A_full) to get list of variables
% this is symbolic, so any symbolic operations are valid!

A01                     = simplify(A1);
A02(q1,q2)              = simplify(A01(q1)*A2(q2));
A03(q1,q2,q3)           = simplify(A02(q1,q2)*A3(q3));
A0ee(q1,q2,q3)          = simplify(A03(q1,q2,q3)*Aee);

end
