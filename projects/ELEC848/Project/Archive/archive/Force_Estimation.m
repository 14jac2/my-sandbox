function [predicted_Fext] = Force_Estimation(K, Y_func, T, idx, Theta_BIG, Jac)

%**** UNCOMMENT THE FOLLOWING TO TEST ESTIMATION ON OUR DATA *****%

% % Find file to test against regressed force parameters
% [file,path] = uigetfile;
% if isequal(file,0)
%     return
% else
%     filename = fullfile(path,file);
% end
% 
% % Open .mat data file and check for the important datasets
% load(filename);
% 
% new_q = smoothdata(q, 'gaussian', 100)*pi/180;
% new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
% new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;
% new_force = smoothdata(force, 'gaussian', 100);
% new_voltage = smoothdata(voltage, 'gaussian', 100);
% 
% % Build our Y and Fext matrices
% first = 1;
% last = length(voltage);
% num_params = length(T);
% first = first + round(last*0.2);
% last = first + 1000;
% 
% Y_BIG = zeros(3*(last-first+1),num_params);
% Fext = zeros(3*(last-first+1),1);
% TAU_BIG = zeros(3*(last-first+1),1);
% 
% for i = first : last
%     row = 3*(i-first) + 1;
%     Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
%                                 new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
%                                                  new_q(i,2),      new_q(i,3));                                        
%     TAU_BIG(row:row+2) = new_voltage(i,1:3).';
% end
% 
% % Take only the important (linearly independent) columns of Y
% L_BIG = Y_BIG(:,idx);
% 
% % Perform prediction on dynamics related voltage
% dynamics = L_BIG * Theta_BIG;
% 
% % Define Jacobian function for forces (no torques included here)
% Jac_p = Jac(1:3,1:3);
% Jp_func = matlabFunction(Jac_p);
% 
% first = 1;
% last = length(voltage);
% num_params = length(T);
% first = first + round(last*0.2);
% last = first + 1000;
% 
% % Perform force estimation
% predicted_Fext = zeros(3*(last-first+1),1);
% 
% for i = first : last
%     row = 3*(i-first) + 1;
%     J_p = Jp_func(new_q(i,1), new_q(i,2), new_q(i,3));
%     Voltage = new_voltage(i,1:3).';
%     Fext(row:row+2) = new_force(i,1:3); % Assuming first three correspond with forces not torques
%     predicted_Fext(row:row+2) = K*transpose(inv(J_p))*(Voltage - dynamics(row:row+2));
% end
% 
% % Plot
% 
% Fext = reshape(Fext,3,[]).';
% predicted_Fext = reshape(predicted_Fext,3,[]).';
% 
% TAU_BIG = reshape(TAU_BIG,3,[]).';
% dynamics = reshape(dynamics,3,[]).';
% 
% figure(2)
% subplot(3,1,1)
% plot(TAU_BIG(:,1) - dynamics(:,1));
% legend('actual','predicted')
% subplot(3,1,2)
% plot(TAU_BIG(:,2) - dynamics(:,2));
% legend('actual','predicted')
% subplot(3,1,3)
% plot(TAU_BIG(:,3) - dynamics(:,3));
% title('Difference in voltage');
% legend('actual','predicted');
% 
% figure(3)
% subplot(3,1,1)
% plot(Fext(:,1));
% hold on
% plot(predicted_Fext(:,1));
% hold off
% legend('actual','predicted')
% subplot(3,1,2)
% plot(Fext(:,2));
% hold on
% plot(predicted_Fext(:,2));
% hold off
% legend('actual','predicted')
% subplot(3,1,3)
% plot(Fext(:,3));
% hold on
% plot(predicted_Fext(:,3));
% hold off
% title('Predicted Forces');
% legend('actual','predicted')



%% ATTEMPT 2
% The following code attempts force estimation using the previous years
% data


% Find file to test against regressed force parameters
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

% Open .mat data file and check for the important datasets
load(filename);

% Find file to test against regressed force parameters
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

% Open .mat data file and check for the important datasets
load(filename);

new_q = smoothdata(Joint_angles, 'gaussian', 100)*pi/180;
new_q_dot = diff(new_q);
new_q_ddot = diff(new_q_dot);
new_voltage = smoothdata(Voltages, 'gaussian', 100);

% Build our Y and Fext matrices
first = 1;
last = length(Joint_angles);
num_params = length(T);
first = first + round(last*0.2);
last = first + 1000;

Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,2), new_q_ddot(i,3), new_q_ddot(i,4), ...
                                new_q_dot(i,2),  new_q_dot(i,3),  new_q_dot(i,4), ...
                                                 new_q(i,3),      new_q(i,4));                                        
    TAU_BIG(row:row+2) = new_voltage(i,2:4).';
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);

% Perform prediction on dynamics related voltage
dynamics = L_BIG * Theta_BIG;

% Define Jacobian function for forces (no torques included here)
Jac_p = Jac(1:3,1:3);
Jp_func = matlabFunction(Jac_p);

first = 1;
last = length(Joint_angles);
num_params = length(T);
first = first + round(last*0.2);
last = first + 1000;

% Perform force estimation
predicted_Fext = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    J_p = Jp_func(new_q(i,2), new_q(i,3), new_q(i,4));
    Voltage = new_voltage(i,2:4).';
    predicted_Fext(row:row+2) = K*transpose(inv(J_p))*(Voltage - dynamics(row:row+2));
end

% Plot

figure(2)
subplot(3,1,1);
plot(new_q(:,2));
title('Joint 1 Angle');
subplot(3,1,2);
plot(new_q_dot(:,2));
title('Joint 1 Velocity');
subplot(3,1,3);
plot(new_q_ddot(:,2));
title('Joint 1 Acceleration');
sgtitle('Joint 1')

figure(3)
subplot(3,1,1);
plot(new_q(:,3));
title('Joint 2 Angle');
subplot(3,1,2);
plot(new_q_dot(:,3));
title('Joint 2 Velocity');
subplot(3,1,3);
plot(new_q_ddot(:,3));
title('Joint 2 Acceleration');
sgtitle('Joint 2')

figure(4)
subplot(3,1,1);
plot(new_q(:,4));
title('Joint 3 Angle');
subplot(3,1,2);
plot(new_q_dot(:,4));
title('Joint 3 Velocity');
subplot(3,1,3);
plot(new_q_ddot(:,4));
title('Joint 3 Acceleration');
sgtitle('Joint 3')

TAU_BIG = reshape(TAU_BIG,3,[]).';
dynamics = reshape(dynamics,3,[]).';

figure(5)
subplot(3,1,1)
plot(TAU_BIG(:,1));
subplot(3,1,2)
plot(TAU_BIG(:,2));
subplot(3,1,3)
plot(TAU_BIG(:,3));
sgtitle('Voltage');

predicted_Fext = reshape(predicted_Fext,3,[]).';
figure(6)
subplot(3,1,1)
plot(predicted_Fext(:,1));
subplot(3,1,2)
plot(predicted_Fext(:,2));
subplot(3,1,3)
plot(predicted_Fext(:,3));
sgtitle('Predicted Forces');

end