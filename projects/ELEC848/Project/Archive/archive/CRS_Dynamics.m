%% ELEC 848 CRS Dynamics
% Thomas Huckell March 7 2020

syms m1 m2 m2 m3 lc1 lc2 lc3 I1xx I1yy I1zz ...
     I2xx I2yy I2zz I3xx I3yy I3zz ;           % linkage properties
syms q1 q2 q3;         % joint Variables
syms Dq1 Dq2 Dq3 ;   % joint velocities
syms DDq1 DDq2 DDq3 ;

syms g ;   % gravitational accel


Theta_0 = [m1 lc1 I1xx I1yy I1zz m2 lc2 I2xx I2yy I2zz m3 lc3 I3xx I3zz];

I1 = diag([I1xx I1yy I1zz]);
I2 = diag([I2xx I2yy I2zz]);
I3 = diag([I3xx I3yy I3zz]);

q   = [q1 q2 q3 ];
Dq  = [Dq1 Dq2 Dq3 ];
DDq = [DDq1 DDq2 DDq3 ];

M = [m1 m2 m3];
I = [I1 I2 I3 ];

G = [0;0;g];

syms a2 a3
% DH parameters
d_i = [0 0 0 ];
a_i = [0 305 330];
al_i = [pi/2 0 0];

% Generate symbolic functions of the homogenous transformation amtrix
[T01,T02,T03,T0ee] = A_transforms(d_i,al_i,a_i);

syms A01 A02 A03 A0ee

A01 = T01(q1);
A02 = T02(q1,q2);
A03 = T03(q1,q2,q3);
A0ee = T0ee(q1,q2,q3);


%%

Z = sym(zeros(3,3));

Z(1,3) = 1;
Z(2,:) = A01(1:3,3);
Z(3,:) = A02(1:3,3);

P = sym(zeros(3,3));

P(1,:) = A01(1:3,4);
P(2,:) = A02(1:3,4);
P(3,:) = A03(1:3,4);

P_com = sym(zeros(3,3));

P_com(1,:) = 0;
P_com(2,:) = P(1,:) + lc2*A02(1:3,1).';
P_com(3,:) = P(2,:) + lc3*A03(1:3,1).';

Jac = sym(zeros(6,3));

 for i = 1:3
     if i == 1
        Jac(:,i)= simplify([ cross(Z(i,:),(A03(1:3,4).' )), Z(i,:)]);
     else
        Jac(:,i)= simplify([ cross(Z(i,:),(A03(1:3,4).' - P(i-1,:))), Z(i,:)]); 
     end
     
     
 end
 
 Omg = sym(zeros(3,3));
 
 Omg(1,:) = Dq1*Jac(4:6,1);
 Omg(2,:) = Omg(1,:) + Dq2*Jac(4:6,2).';
 Omg(3,:) = Omg(2,:) + Dq3*Jac(4:6,3).';
 
 DP = sym(zeros(3,3));
 
 DP(1,:) = Dq1*Jac(1:3,1);
 DP(2,:) = DP(1,:) + Dq2*Jac(1:3,2).';
 DP(3,:) = DP(2,:) + Dq3*Jac(1:3,3).';
 
 DP_com = sym(zeros(3,3));
 
 DP_com(2,:) = DP(1,:) + cross(Omg(2,:),-lc2*A02(1:3,1).');
 DP_com(3,:) = DP(2,:) + cross(Omg(3,:), lc3*A03(1:3,3).');

 
 T = sym(zeros(1));
 U = sym(zeros(1));
%%
 for i = 1:3
     
     U = U + -M(i)*(G.'*P_com(i,:).');
     T = T + 1/2*Omg(i,:)*I(1:3,(3*(i-1)+1):(3*(i-1)+3))*Omg(i,:).' + M(i)*DP_com(i,:)*DP_com(i,:).';
     
 end
 
L = T-U;

DdelL_qdot = sym(zeros(3,1));
delL_q     = sym(zeros(3,1));

%%
for i = 1:3
    
    DdelL_qdot(i) = LagrangeTimeDerivative(L,Dq(i));
    delL_q(i)     = diff(L,q(i));
    delU_q(i)     = diff(U,q(i));
end

MASS    = sym(zeros(3,3));
GRAVITY = sym(zeros(3,1));

for i = 1:3
    for j = 1:3
        
        M_coef = coeffs(DdelL_qdot(i),DDq(j));
        
        if length(M_coef) > 1
            MASS(i,j) = M_coef(2);
        else
            MASS(i,j) = 0; 
        end
        
    end
    
    G_coef = coeffs(-delL_q(i),g);
    
    if length(G_coef) > 1
        GRAVITY(i,1) = G_coef(2);
    else
        GRAVITY(i,1) = 0;
    end
end

Tau = DdelL_qdot - delL_q;


Coriolis = Tau -MASS*DDq.' -GRAVITY;
%%
C = christoffel(MASS);


%Y(q1,q2,q3,Dq1,Dq2,Dq3,DDq1,DDq2,DDq3,g)
Y_0 = sym(zeros(3,length(Theta_0)));

for i = 1:3
    
    for j = 1:length(Theta_0)
    
    if Theta_0(j) == lc1
        [~,t] = coeffs(Tau(i,:),Theta_0(j));
        temp = t*ones(length(t),1);
        
        y_coef = coeffs(temp,m1);
    
    elseif Theta_0(j) == lc2
        [~,t] = coeffs(Tau(i,:),Theta_0(j));
        temp = t*ones(length(t),1);
        
        y_coef = coeffs(temp,m2);
        
    elseif Theta_0(j) == lc3    
        [~,t] = coeffs(Tau(i,:),Theta_0(j));
        temp = t*ones(length(t),1);
        y_coef = coeffs(temp,m3);
        
    else
        y_coef = coeffs(Tau(i,:),Theta_0(j));
    
    end
         if length(y_coef) > 1
            Y_0(i,j) = y_coef(2);
         else
            Y_0(i,j) = 0; 
         end
         
    end
end

zeroCol = find(sum(abs(Y_0)) == 0);
nonZeroCol = find(sum(Y_0) ~= 0);

Theta = sym(zeros(length(nonZeroCol),1));
Y     = sym(zeros(3,length(nonZeroCol)));

for i = 1:length(nonZeroCol)
    
    Theta(i) = Theta_0(nonZeroCol(i));
    Y(:,i)   = Y_0(:,nonZeroCol(i));
    
end

Y_n = matlabFunction(Y); % @(DDq1,DDq2,DDq3,Dq1,Dq2,Dq3,a2,a3,g,lc2,lc3,q1,q2,q3)




