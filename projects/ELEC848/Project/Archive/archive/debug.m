syms m1 m2 m2 m3 lc1 lc2 lc3 I1xx I1yy I1zz ...
     I2xx I2yy I2zz I3xx I3yy I3zz ;           % linkage properties
syms q1 q2 q3;         % joint Variables
syms Dq1 Dq2 Dq3 ;   % joint velocities
syms DDq1 DDq2 DDq3 ;

theta_default = [m1 lc1 I1xx I1yy I1zz m2 lc2 I2xx I2yy I2zz m3 lc3 I3xx I3yy I3zz];

q   = [q1 q2 q3 ];
Dq  = [Dq1 Dq2 Dq3 ];
DDq = [DDq1 DDq2 DDq3 ];


%% Get Lagrange equation

%%%%% Jack code modification
[U, T] = Lagrange;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L = T-U;

DdelL_qdot = sym(zeros(3,1));
delL_q     = sym(zeros(3,1));

for i = 1:3
    DdelL_qdot(i) = LagrangeTimeDerivative(L,Dq(i));
    delL_q(i)     = diff(L,q(i));
end

Tau = DdelL_qdot - delL_q;
newTau = simplify(Tau);
TAU = matlabFunction(newTau);

%% Generate manipulator data assuming simple motion
t_step = 1e-3;

I2xx = 2;
I3xx = 3;
I1yy = 1;
I2yy = 2;
I3yy = 3;
I2zz = 2;
I3zz = 3;

lc2 = .25;
lc3 = .35;
m2 = 2;
m3 = 3;

t = 0:t_step:1;
L = length(t);

q = [sin(2*pi*t+0.3); 1.4*sin(1.4*2*pi*t+1.4); 1.7*cos(1.7*2*pi*t)].';
Dq = 2*pi*[cos(2*pi*t+0.3); (1.4^2)*cos(1.4*2*pi*t+1.4); -(1.7^2)*sin(1.7*2*pi*t)].';
DDq = -4*pi^2*[sin(2*pi*t+0.3); (1.4^3)*sin(1.4*2*pi*t+1.4); -(1.7^3)*cos(1.7*2*pi*t)].';

tau = zeros(L,3);

for i = 1:L
    
    tau(i,:) = TAU(DDq(i,1),DDq(i,2),DDq(i,3),...
                    Dq(i,1),  Dq(i,2), Dq(i,3),...
                    I2xx,I3xx,I1yy,I2yy,I3yy,I2zz,I3zz,lc2,lc3,m2,m3,...
                               q(i,2),  q(i,3));
end

%% Get the paramaterized matrix Y
[Y, T] = Dynamics;

% Convert Y to a MATLAB function
Y_func = matlabFunction(Y);
disp(symvar(Y));


%% Build Y matrix
Y_BIG = zeros(3*L,length(T));

for i = 1:L
    row = 3*(i-1)+1;
    Y_BIG(row:row+2,:) = Y_func(DDq(i,1),DDq(i,2),DDq(i,3), ...
                                 Dq(i,1), Dq(i,2), Dq(i,3), ...
                                           q(i,2),  q(i,3));
end

TAU_BIG = reshape(tau.',[3*L, 1]);

%% Theta equation
Theta_BIG = ((Y_BIG.' * Y_BIG) \ Y_BIG.') * TAU_BIG;

for i = 1:length(T)
    fprintf('%s\t%f\n',T(i),Theta_BIG(i))
end

