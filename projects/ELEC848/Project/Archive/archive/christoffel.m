function C = christoffel(M)
% generate the christoffel form based on the mass matrix.

    syms m1 m2 m3 l1 l2 l3  lc1 lc2 lc3  I1 ...
         I2 I3  ;           % linkage properties
    syms q1 q2 q3;         % joint Variables
    syms Dq1 Dq2 Dq3 ;   % joint velocities

    Q = [q1; q2; q3];
    DQ = [Dq1; Dq2; Dq3];


    [dim,~] = size(M);      % determine the dimension of Mass matrix

    %initialize the multidimensional array of christoffel symbols
    c = sym(zeros(dim));        
    C = sym(zeros(dim));

    %%
    % generate dim-by-dim-by-dim array of zeros
    for i = 2:dim

        c(:,:,i) = sym(zeros(dim));

    end


    for k = 1:dim

        for j = 1:dim

            for i = 1:dim

                c(i,j,k) = (1/2) * (diff( M(k,j) , Q(i)) + ...
                           diff( M(k,i) , Q(j)) - diff( M(i,j) , Q(k)));
            end

        end

    end

    for k = 1:dim

        for j = 1:dim

            C(k,j) = c(:,j,k).'*DQ;

        end

    end

end


