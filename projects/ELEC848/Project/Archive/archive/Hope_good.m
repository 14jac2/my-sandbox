%% ELEC 848 CRS Dynamics
% Derrick Mansourian March 14 2020

%% Constants

a2 = 305;
a3 = 330;
g = 9.81;

%% Defining a structure for the totatl Y matrix

YTotal = sym(zeros(3*length(voltage),8));

 for i = 1 : length(voltage)

    Y = Y_n(q_ddot(i,1), q_ddot(i,2), q_ddot(i,3), q_dot(i,1), q_dot(i,2), q_dot(i,3), a2, a3, g, lc2, lc3, q(i,1),q(i,2),q(i,3));
    
        for j = 1 : 3 
            
            YTotal(3*(i-1) + j,:) = Y (j,:);

        end
        
 end

 %% Theta equation
 
%  ThetaT = sym(zeros(8,1));
 
 ThetaT = ((YTotal.' * YTotal) \ YTotal.') * volt;