clear all
clc

%% Dynamic regression

% First we create stack of Y matrices using dynamics
Y_stack_TH;
 
% Reduce the matrix based on linearly independent columns
[L, new_Theta, idx] = Model_Reduction(Y_BIG, T);

% Perform regression to find theta
Theta_BIG = ((L.' * L) \ L.') * TAU_BIG;

%% Dynamic testing

% Find file to test against regressed dynamics
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

% Open .mat data file and check for the important datasets
load(filename);

new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;

new_voltage = smoothdata(voltage, 'gaussian', 100);

% Build our new Y matrix
first = 1;
last = length(voltage);
num_params = length(T);
first = first + round(last*0.2);
last = first + 1000;

Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);

% Perform prediction
predicted_voltage = L_BIG * Theta_BIG;

% Plot
TAU_BIG = reshape(TAU_BIG,3,[]).';
predicted_voltage = reshape(predicted_voltage,3,[]).';
plot(TAU_BIG);
hold on
plot(predicted_voltage);
hold off


%% Force regression

% Perform regression to find motor parameter K
K = Force_Regression(Y_func, T, idx, Theta_BIG);

%% Force testing
% Source new file for testing

% Predict force