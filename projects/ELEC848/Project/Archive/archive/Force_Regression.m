function K = Force_Regression(Y_func, T, idx, Theta_BIG, Jac)
%% Acquire Y and Fext
% Source file for regression
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

% Open .mat data file and check for the important datasets
load(filename);

% Smooth data
new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;
new_voltage = smoothdata(voltage, 'gaussian', 100);
new_force = smoothdata(force, 'gaussian', 100);

% Build our Y and Fext matrices
first = 1;
last = length(voltage);
num_params = length(T);
first = first + round(last*0.2);
last = first + 1000;

Y_BIG = zeros(3*(last-first+1),num_params);
Fext = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
    Fext(row:row+2) = new_force(i,1:3); % Assuming first three correspond with forces not torques
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);

% Perform prediction on dynamics related voltage
dynamics = L_BIG * Theta_BIG;

%% Acquire other equation parameters
% Define Jacobian function for forces (no torques included here)
Jac_p = Jac(1:3,1:3);
Jp_func = matlabFunction(Jac_p);

% Define X for regression, where Fext = K(transpose(J^-1)(V-dynamics) = KX
X = zeros(3*(last-first+1),1);

% Populate X
for i = first : last
    row = 3*(i-first) + 1;
    J_p = Jp_func(new_q(i,1), new_q(i,2), new_q(i,3));
    Voltage = new_voltage(i,1:3).';
    
    X(row:row+2) = transpose(inv(J_p))*(Voltage - dynamics(row:row+2));
end

%% Regression
% Perform regression
K = regress(Fext, X);


end