
%% Preample
% Get test data
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end
% Open .mat data file and check for the important datasets
load(filename);

%%%%% TEMP MODIFYING INCOMING DATA %%%%%
% new_q_dot = diff(q)/(1/1000);
% new_q_ddot = diff(q_dot)/(1/1000);
new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;

new_voltage = smoothdata(voltage, 'gaussian', 100);

%% Get the paramaterized matrix Y
[Y, T] = Dynamics_withFric;

% Convert Y to a MATLAB function
Y_func = matlabFunction(Y);
disp(symvar(Y));

%% Constants
g = 9.81;
first = 1;
last = length(voltage);
num_params = length(T);


%% Build Y matrix
Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last

    row = 3*(i-first) + 1;

    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
%     Y_BIG(row:row+2,:) = Y_func(q_ddot(i,1), q_ddot(i,2), q_ddot(i,3), ...
%                                 q_dot(i,1),  q_dot(i,2),  q_dot(i,3), ...
%                                              q(i,2),      q(i,3));
                            
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
    
end

[L, new_theta, idx] = Model_Reduction(Y_BIG, T);

% sympref('FloatingPointOutput',true)

new_theta = new_theta;

Theta_BIG = ((L.' * L) \ L.')* TAU_BIG;

 for i = 1:length(new_theta)
   fprintf('%s\t%f\n',new_theta(i),Theta_BIG(i))
 end
%% Dynamic testing

% Find file to test against regressed dynamics
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

% Open .mat data file and check for the important datasets
load(filename);

new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;
new_voltage = smoothdata(voltage, 'gaussian', 100);

% Build our new Y matrix
first = 1;
last = length(voltage);
num_params = length(T);
first = first + round(last*0.2);
last = first + 4000;

Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);

% Perform prediction
predicted_voltage = L_BIG * Theta_BIG;

% Plot

TAU_BIG = reshape(TAU_BIG,3,[]).';
predicted_voltage = reshape(predicted_voltage,3,[]).';

subplot(3,1,1)
plot(time(first:last)-time(first),TAU_BIG(:,1));
hold on
plot(time(first:last)-time(first),predicted_voltage(:,1));
hold off
legend('V_1 actual','V_1 predicted')
ylabel('voltage [V]')
subplot(3,1,2)
plot(time(first:last)-time(first),TAU_BIG(:,2));
hold on
plot(time(first:last)-time(first),predicted_voltage(:,2));
hold off
legend('V_2 actual','V_2 predicted')
ylabel('voltage [V]')
subplot(3,1,3)
plot(time(first:last)-time(first),TAU_BIG(:,3));
hold on
plot(time(first:last)-time(first),predicted_voltage(:,3));
hold off
legend('V_3 actual','V_3 predicted')
xlabel('time [s]')
ylabel('voltage [V]')


RMSerror = sqrt( sum( (TAU_BIG - predicted_voltage).^2 )/length(TAU_BIG(:,1)));
%% Fore Regression

Jac = Jacobian;

K = Force_Regression(Y_func, T, idx, Theta_BIG, Jac);

%% Force Estimation

% 100g data set
% When running this make sure the 4th data set selected is a 100 g run
predicted_Fext100 = Force_Estimation_100(K, Y_func, T, idx, Theta_BIG, Jac);

% 1 kg data set
% Ensure the 5th and 6th data sets are last years Voltage and angles 
predicted_Fext1k = Force_Estimation_1k(K, Y_func, T, idx, Theta_BIG, Jac);

%% Functions

%[THETA] = dynamicIdentify(Y_func,T,new_q,new_q_dot,new_q_ddot,new_voltage,10000);


function [THETA] = dynamicIdentify(Y_fun,T,q,q_dot,q_ddot,V,sampleSize)
    
    
    last = length(V);                 %determine the size of the supplied data set
    
    N = floor(last/sampleSize);             %determine the amount of averages of dynamic parameters
    
    num_params = length(T);
    
    THETA = zeros(num_params,N);              %create array of dynamic parameter estimation
 
    
    start = zeros(1,N);
    
    start(1) = floor((last - N*sampleSize)/2)-1;
    
    fin = zeros(1,N);
    
    for i = 1:N-1
        
        fin(i) = start(i) + sampleSize - 1;
        start(i+1) = fin(i) + 1;
    end
    
    fin(end) = start(end) + sampleSize - 1;
   

    %% Build Y matrix
    for j = 1:N
    Y_BIG = zeros(3*(sampleSize),num_params);
    TAU_BIG = zeros(3*(sampleSize),1);
        for i = start(j) : fin(j)



            row = 3*(i-start(j)) + 1;

            Y_BIG(row:row+2,:) = Y_fun(q_ddot(i,1), q_ddot(i,2), q_ddot(i,3), ...
                                        q_dot(i,1), q_dot(i,2),  q_dot(i,3), ...
                                                         q(i,2),      q(i,3));

            TAU_BIG(row:row+2) = V(i,1:3).';

        end
        Theta_BIG = ((Y_BIG.' * Y_BIG) \ Y_BIG.') * TAU_BIG;
        
        for i = 1:num_params
            fprintf('%s\t%f\n',T(i),Theta_BIG(i))
        end

        THETA(:,j) = Theta_BIG;
    end
end




