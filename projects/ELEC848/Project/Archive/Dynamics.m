%% ELEC 848 CRS Dynamics
% Thomas Huckell March 7 2020
% Modified by Thomas Sears March 20 2020


function [Y, THETA] = Dynamics

syms m1 m2 m2 m3 lc1 lc2 lc3 I1xx I1yy I1zz ...
     I2xx I2yy I2zz I3xx I3yy I3zz ;           % linkage properties
syms q1 q2 q3;         % joint Variables
syms Dq1 Dq2 Dq3 ;   % joint velocities
syms DDq1 DDq2 DDq3 ;



q   = [q1 q2 q3 ];
Dq  = [Dq1 Dq2 Dq3 ];
DDq = [DDq1 DDq2 DDq3 ];



theta_default = [m1 lc1 I1xx I1yy I1zz m2 lc2 I2xx I2yy I2zz m3 lc3 I3xx I3yy I3zz];


%%%%% Jack code modification
[U, T] = Lagrange;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L = T-U;

DdelL_qdot = sym(zeros(3,1));
delL_q     = sym(zeros(3,1));

for i = 1:3
    DdelL_qdot(i) = LagrangeTimeDerivative(L,Dq(i));
    delL_q(i)     = diff(L,q(i));
end

Tau = DdelL_qdot - delL_q;



newTau = simplify(Tau);


%% Parameters search and extraction
vec = cell(3,1);
params = cell(3,1);

for ii = 1:3
    [vec{ii},params{ii}]=coeffs(newTau(ii),theta_default);
end

[~, THETA] = coeffs(sum(unique([params{:}])), theta_default);
count = length(THETA);

Y = sym(zeros(3,count));

for ii = 1:3    
    for jj = 1:count
        if ismember(THETA(jj),params{ii})
            idx = logical(THETA(jj)==params{ii});
            Y(ii,jj) = vec{ii}(idx);
        end
    end
end


%% Check properties
MASS    = sym(zeros(3,3));
%GRAVITY = sym(zeros(3,1));

for i = 1:3
    for j = 1:3
        
        M_coef = coeffs(DdelL_qdot(i),DDq(j));
        
        if length(M_coef) > 1
            MASS(i,j) = M_coef(2);
        else
            MASS(i,j) = 0; 
        end
        
    end     
%     G_coef = coeffs(-delL_q(i),g);
%         if length(G_coef) > 1
%         GRAVITY(i,1) = G_coef(2);
%         else
%         GRAVITY(i,1) = 0;
%         end
end
