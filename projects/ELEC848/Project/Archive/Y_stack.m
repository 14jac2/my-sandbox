%% ELEC 848 CRS Dynamics
% Derrick Mansourian March 14 2020
% Modified by Thomas Sears March 20 2020

%% Preample
% Get test data
[file,path] = uigetfile;
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end
% Open .mat data file and check for the important datasets
load(filename);

%%%%% TEMP MODIFYING INCOMING DATA %%%%%
% new_q_dot = diff(q)/(1/1000);
% new_q_ddot = diff(q_dot)/(1/1000);
new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;

new_voltage = smoothdata(voltage, 'gaussian', 100);

%% Get the paramaterized matrix Y
[Y, T, M] = Dynamics;

% Convert Y to a MATLAB function
Y_func = matlabFunction(Y);
disp(symvar(Y));

%% Constants
g = 9.81;
first = 1;
last = length(voltage);
num_params = length(T);



% prune even more data from the set
first = first + round(last*0.2);
% last = last - round(last*0.2);
last = first + 2000;

%% Build Y matrix
Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last

    row = 3*(i-first) + 1;

    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
%     Y_BIG(row:row+2,:) = Y_func(q_ddot(i,1), q_ddot(i,2), q_ddot(i,3), ...
%                                 q_dot(i,1),  q_dot(i,2),  q_dot(i,3), ...
%                                              q(i,2),      q(i,3));
                            
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
    
end

disp(rank(Y_BIG));

%% Theta equation
Theta_BIG = ((Y_BIG.' * Y_BIG) \ Y_BIG.') * TAU_BIG;

for i = 1:length(T)
    fprintf('%s\t%f\n',T(i),Theta_BIG(i))
end


