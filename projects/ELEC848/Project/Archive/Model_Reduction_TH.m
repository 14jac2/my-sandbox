
function [L, new_theta, idx] = Model_Reduction_TH(Y, Theta)
    % Check total number of columns of Y
    [numRows,numCols] = size(Y);

    % Determine how manY_BIG theta parameters there should be
    num_Theta = rank(Y);

    % Initialize L and Theta as correct size to avoid rank deficiency (L is new Y)
    L = zeros(numRows, num_Theta);
    temp = L;
    new_theta = sym(zeros(num_Theta,1));
    idx = zeros(num_Theta, 1);
    
    % Counter for columns added to L
    j = 1;

    % Rank tracker
    r = 0;

    % Loop through each column of Y, add said column to L (in this case temp)
    % check the new rank, if increased then this column is linearly
    % independent, otherwise it is a linear combination
    
    for i = 1:numCols % Loop through all Y columns
       r_prev = r; % Save current rank
       temp(:,j) = Y(:,i); % Add next column
       r = rank(temp(:,1:j)); % Save new rank
       if r > r_prev % If rank is increased
           L(:,j) = temp(:,j); % Add new column to L
           new_theta(j) = Theta(i); % Add theta to new theta
           idx(j) = i; % index of the saved Y columns in L
           j = j + 1; % Increment size of L
       else % Rank stays the same - linearly dependent column
           alpha = (L(:,1:j-1).'*L(:,1:j-1)) \ L(:,1:j-1).' * temp(:,j); % Equation for linear combination
           temp(:,j) = []; % Remove the column
           lin_comb_theta = new_theta(1:j-1).*alpha; % Theta as function of others
           new_theta(1:j-1) = new_theta(1:j-1) + lin_comb_theta;
       end
    end
end
    
