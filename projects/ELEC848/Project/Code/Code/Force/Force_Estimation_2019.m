function [predicted_Fext] = Force_Estimation_2019(dataPath,K, Y_func, T, idx, Theta_BIG, Jac)
% The following code attempts force estimation using the data from 2019

% Find voltage file to test against regressed force parameters
Path = strcat(dataPath,'/2019_voltages.mat');

[file,path] = uigetfile(Path,'SELECT 2019 VOLTAGE FILE');
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

load(filename);

% Find joint angle file to test against regressed force parameters
Path = strcat(dataPath,'/2019_jointangles.mat');

[file,path] = uigetfile(Path,'SELECT 2019 JOINT ANGLES FILE');
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end

load(filename);

% Smooth data
new_q = smoothdata(Joint_angles, 'gaussian', 100)*pi/180;
new_q_dot = diff(new_q);
new_q_ddot = diff(new_q_dot);
new_voltage = smoothdata(Voltages, 'gaussian', 100);

% Build our Y and T matrices
first = 1;
last = length(Joint_angles);
num_params = length(T);
first = first + round(last*0.2);
last = first + 1000;

Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,2), new_q_ddot(i,3), new_q_ddot(i,4), ...
                                new_q_dot(i,2),  new_q_dot(i,3),  new_q_dot(i,4), ...
                                                 new_q(i,3),      new_q(i,4));                                        
    TAU_BIG(row:row+2) = new_voltage(i,2:4).';
end

% Take only the important (linearly independent) columns of Y
L_BIG = Y_BIG(:,idx);

% Perform prediction on dynamics related voltage
dynamics = L_BIG * Theta_BIG;

% Define Jacobian function for forces (no torques included here)
Jac_p = Jac(1:3,1:3);
J_p_func = matlabFunction(Jac_p);

first = 1;
last = length(Joint_angles);
first = first + round(last*0.2);
last = first + 1000;

% Perform force estimation
predicted_Fext = zeros(3*(last-first+1),1);

for i = first : last
    row = 3*(i-first) + 1;
    J_p = J_p_func(new_q(i,2), new_q(i,3), new_q(i,4));
    Voltage = new_voltage(i,2:4).';
    predicted_Fext(row:row+2) = K*inv(transpose(J_p))*(Voltage - dynamics(row:row+2));
end

%% Plot

% Plot the joint 1 trajectoru
figure
subplot(3,1,1);
plot(new_q(:,1));
title('Joint 1 Angle');
subplot(3,1,2);
plot(new_q_dot(:,1));
title('Joint 1 Velocity');
subplot(3,1,3);
plot(new_q_ddot(:,1));
title('Joint 1 Acceleration');
sgtitle('Joint 1')

% Plot the joint 2 trajectoru
figure
subplot(3,1,1);
plot(new_q(:,2));
title('Joint 2 Angle');
subplot(3,1,2);
plot(new_q_dot(:,2));
title('Joint 2 Velocity');
subplot(3,1,3);
plot(new_q_ddot(:,2));
title('Joint 2 Acceleration');
sgtitle('Joint 2')

% Plot the joint 3 trajectoru
figure
subplot(3,1,1);
plot(new_q(:,3));
title('Joint 3 Angle');
subplot(3,1,2);
plot(new_q_dot(:,3));
title('Joint 3 Velocity');
subplot(3,1,3);
plot(new_q_ddot(:,3));
title('Joint 3 Acceleration');
sgtitle('Joint 3')

TAU_BIG = reshape(TAU_BIG,3,[]).';
dynamics = reshape(dynamics,3,[]).';

% Plot the actual and predicted voltages
figure
subplot(3,1,1)
plot(TAU_BIG(:,1));
hold on
plot(dynamics(:,1));
hold off
ylabel('Voltage (V)');
legend('Actual','Predicted Dynamics');
title('Joint 1');
subplot(3,1,2)
plot(TAU_BIG(:,2));
hold on
plot(dynamics(:,2));
hold off
ylabel('Voltage (V)');
title('Joint 2');
subplot(3,1,3)
plot(TAU_BIG(:,3));
hold on
plot(dynamics(:,3));
hold off
ylabel('Voltage (V)');
xlabel('Time');
title('Joint 3');
sgtitle('Voltage');

time = Voltages;
predicted_Fext = reshape(predicted_Fext,3,[]).';

% Plot the estimated forces at EE
figure
subplot(3,1,1)
plot(time(first:last,1)-time(first,1),predicted_Fext(:,1));
ylabel('Force (N)');
xlabel('Time (s)');
title('Fxee');
ylim([-25 1]);
subplot(3,1,2)
plot(time(first:last,1)-time(first,1),predicted_Fext(:,2));
ylabel('Force (N)');
xlabel('Time (s)');
ylim([-1 1]);
title('Fyee');
subplot(3,1,3)
plot(time(first:last,1)-time(first,1),predicted_Fext(:,3));
ylabel('Force (N)');
xlabel('Time (s)');
ylim([-1 15]);
title('Fzee');
sgtitle('Predicted Forces 1 kg');

% Calculating norm for visualization of singularities
n = zeros(length(predicted_Fext(:,1)));
% Calculating norm
for i=1:length((predicted_Fext(:,1)))
    n(i) = norm(predicted_Fext(i,:));
end

% Plot the norm
figure
plot(n);
ylabel('Force (N)');
title('Norm of Predicted Forces');

% Redefining tranform to acquire vertical component of force
% DH parameters
d_i = [0 0 0];
a_i = [0 305 330+76]*1e-3;
al_i = [pi/2 0 0];

% Generate symbolic functions of the homogenous transformation amtrix
[T01,T02,T03,T0ee] = Homogenous_transforms(d_i,al_i,a_i);

syms A01 A02 A03 A0ee;
syms q1 q2 q3;         % joint Variables

A0ee = T0ee(q1,q2,q3);
R0ee = A0ee(1:3,1:3);
R0ee_func = matlabFunction(R0ee);

predicted_Fext_base = zeros(length(predicted_Fext(:,1)), 3);
% Converting to base frame
for i=1:length((predicted_Fext(:,1)))
    R = R0ee_func(new_q(i,2), new_q(i,3), new_q(i,4));
    predicted_Fext_base(i, :) = R\(transpose(predicted_Fext(i,:)));
end

% Plot the forces with respect to the base frame
figure
subplot(3,1,1)
plot(time(first:last,1)-time(first,1),predicted_Fext_base(:,1));
ylabel('Force (N)');
xlabel('Time (s)');
ylim([-1 1]);
title('Fx');
subplot(3,1,2)
plot(time(first:last,1)-time(first,1),predicted_Fext_base(:,2));
ylabel('Force (N)');
xlabel('Time (s)');
ylim([-15 0]);
title('Fy');
subplot(3,1,3)
plot(time(first:last,1)-time(first,1),predicted_Fext_base(:,3));
ylabel('Force (N)');
xlabel('Time (s)');
ylim([-25 0]);
title('Fz');
sgtitle('Predicted Forces 1 kg, in Base Frame');


end