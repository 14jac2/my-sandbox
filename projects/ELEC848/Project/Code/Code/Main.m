%% ELEC 848 CRS A465
% Jack Caldwell | Thomas Huckell | Thomas Sears
% April 24 2020 


%% Set Directories

currentFolder = pwd;
dynPath       = strcat(currentFolder,'/Dynamics and Kinematics');
forcePath     = strcat(currentFolder,'/Force');
dataPath      = strcat(currentFolder,'/Data (CRS A465)/Raw Data');


addpath(dynPath,forcePath)

%% Run Dynamic Model
% Run Dynamic model to obtain symbolic expression for regressor and dynamic
% parameters (You may choose between Dynamics_withFric which obtains the 
% dynamic model with a coulomb and viscous friction model or Dynamics 
% which neglects friction
[Y, T_0] = Dynamics_withFric; 
                         

% Convert Y to a MATLAB function
Y_func = matlabFunction(Y);
disp(symvar(Y));


%% Run Least Square Regression on Free motion Data Set 
% estimate the dynamic parameters: Theta, for the CRS manipulator using
% free motion data, and the Dynamic model

[Theta, idx] = dynamicParameterEst(dataPath,T_0,Y_func);

%% Verify Dynamic Parameter Estimation
% Estimate the voltage expected at the joints for a different data set
% given the previously calculated dynamic parameters.

[RMSerror, predicted_voltage] = dynamicParameterVerify(dataPath,T_0,Y_func,idx,Theta);

%% Run Force Regression on a Weighted Data Set
% The following function determines the motor constant K based on a
% weighted data set with known EE generlized force values
% The estimated and actual voltages will be plotted as the difference
% between the two is used for regression.
Jac = Jacobian;

fprintf('You will be prompted to select 2020 parsed data set with weight for regression\n');
K = Force_Regression(dataPath, Y_func, T_0, idx, Theta, Jac);

%% Run Force Estimation on a Weighted Data Set
% The following function estimates the external forces acting on the end
% effector for a weighted data set with all information located in a single
% data set (2020 data with 100 g)
% The joint information, predicted vs actual voltage, estimated vs actual force at
% EE, norm of estimated force, and force at EE with respect to base frame
% are all plotted
fprintf('You will be prompted to select 2020 parsed data set with weight for estimation\n');
predicted_Fext2020 = Force_Estimation_2020(dataPath, K, Y_func, T_0, idx, Theta, Jac);

% The following function estimates the external forces acting on the end
% effector for a weighted data set with information located in two seperate
% data sets (2019 data with 1 kg)
% The joint information, predicted vs actual voltage, estimated force at
% EE, norm of estimated force, and force at EE with respect to base frame
% are all plotted
fprintf('You will be prompted to select two 2019 data sets with weight for regression\n');
predicted_Fext2019 = Force_Estimation_2019(dataPath, K, Y_func, T_0, idx, Theta, Jac);
