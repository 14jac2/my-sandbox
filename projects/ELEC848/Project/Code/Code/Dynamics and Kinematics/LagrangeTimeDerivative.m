function [ Lt1 ] = LagrangeTimeDerivative(L,Dq_i)

syms DDq1 DDq2 DDq3 
syms Dq1 Dq2 Dq1 Dq3 
syms q1 q2 q3 


DDq  = [DDq1 DDq2 DDq3 ];
Dq   = [Dq1 Dq2 Dq3];
q    = [q1 q2 q3];

delL = simplify(diff(L,Dq_i));

Lt1 = sym(zeros(1));

for i = 1:3

    Lt1 = Lt1 + DDq(i)*simplify(diff(delL,Dq(i))) + Dq(i)*simplify(diff(delL,q(i))) ;
    
end
