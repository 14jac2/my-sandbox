%% ELEC 848 CRS Dynamics
% Thomas Huckell March 7 2020
% Modified by Thomas Sears March 20 2020


function [Y, THETA] = Dynamics_withFric

syms m1 m2 m2 m3 lc1 lc2 lc3 I1xx I1yy I1zz ...
     I2xx I2yy I2zz I3xx I3yy I3zz ;           % linkage properties
syms q1 q2 q3;         % joint Variables
syms Dq1 Dq2 Dq3 ;   % joint velocities
syms DDq1 DDq2 DDq3 ;
syms f_c1 f_c2 f_c3 fv1 fv2 fv3; % colomb and viscous friction factors


q   = [q1 q2 q3 ];
Dq  = [Dq1 Dq2 Dq3 ];
DDq = [DDq1 DDq2 DDq3 ];

f_c = [f_c1 f_c2 f_c3];
f_v = [fv1 fv2 fv3];

Friction = (f_c.*Dq).' + (f_v.*[sign(Dq1) sign(Dq2) sign(Dq3)]).';

%theta_default = [m1 lc1 I1xx I1yy I1zz m2 lc2 I2xx I2yy I2zz m3 lc3 I3xx I3yy I3zz];
theta_default_fric = [m1 lc1 I1xx I1yy I1zz m2 lc2 I2xx I2yy I2zz m3 lc3 I3xx I3yy I3zz f_c1 f_c2 f_c3 fv1 fv2 fv3];

%%%%% Jack code modification
[U, T] = Lagrange;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

L = T-U;

DdelL_qdot = sym(zeros(3,1));
delL_q     = sym(zeros(3,1));

for i = 1:3
    DdelL_qdot(i) = LagrangeTimeDerivative(L,Dq(i));
    delL_q(i)     = diff(L,q(i));
end

Tau = DdelL_qdot - delL_q + Friction;



newTau = simplify(Tau) ;


%% Parameters search and extraction
vec = cell(3,1);
params = cell(3,1);

for ii = 1:3
    [vec{ii},params{ii}]=coeffs(newTau(ii),theta_default_fric);
end

[~, THETA_0] = coeffs(sum(unique([params{:}])), theta_default_fric);
count = length(THETA_0);

Y_0 = sym(zeros(3,count));

for ii = 1:3    
    for jj = 1:count
        if ismember(THETA_0(jj),params{ii})
            idx = logical(THETA_0(jj)==params{ii});
            Y_0(ii,jj) = vec{ii}(idx);
        end
    end
end


zeroCol = find(sum(abs(Y_0)) == 0);
nonZeroCol = find(sum(Y_0) ~= 0);

THETA = sym(zeros(length(nonZeroCol),1));
Y     = sym(zeros(3,length(nonZeroCol)));

for i = 1:length(nonZeroCol)
    
    THETA(i) = THETA_0(nonZeroCol(i));
    Y(:,i)   = Y_0(:,nonZeroCol(i));
    
end


end
