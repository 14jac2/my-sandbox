
function [Theta, idx ] = dynamicParameterEst(dataPath,T,Y_func)
% dynamicParamEst estimates the dynamic parameters Theta based on a dynamic
% model T = Y*Theta. 
% The inputs are:
% dataPath : path to the folder with the CRS data
% T: symbolic expression of the dynamic parameters
% Y_func: symbolic matlab function for the matrix Y


%% Select Data
% Select one of the three free motion data sets 
Path = strcat(dataPath,'/test1_parsed.mat');

[file,path] = uigetfile(Path,'SELECT PARSED DATA: 1-3');
if isequal(file,0)
    return
else
    filename = fullfile(path,file);
end
load(filename);

%% Filter Data
new_q = smoothdata(q, 'gaussian', 100)*pi/180;
new_q_dot = smoothdata(q_dot, 'gaussian', 100)*pi/180;
new_q_ddot = smoothdata(q_ddot, 'gaussian', 100)*pi/180;

new_voltage = smoothdata(voltage, 'gaussian', 100);
%% Constants
first = 1;
last = length(voltage);
num_params = length(T);

%% Build Y matrix
Y_BIG = zeros(3*(last-first+1),num_params);
TAU_BIG = zeros(3*(last-first+1),1);

for i = first : last

    row = 3*(i-first) + 1;

    Y_BIG(row:row+2,:) = Y_func(new_q_ddot(i,1), new_q_ddot(i,2), new_q_ddot(i,3), ...
                                new_q_dot(i,1),  new_q_dot(i,2),  new_q_dot(i,3), ...
                                                 new_q(i,2),      new_q(i,3));
%     Y_BIG(row:row+2,:) = Y_func(q_ddot(i,1), q_ddot(i,2), q_ddot(i,3), ...
%                                 q_dot(i,1),  q_dot(i,2),  q_dot(i,3), ...
%                                              q(i,2),      q(i,3));
                            
    TAU_BIG(row:row+2) = new_voltage(i,1:3).';
    
end



[L, Theta_Sym, idx] = Model_Reduction(Y_BIG, T);

Tau = TAU_BIG;

Theta = ((L.' * L) \ L.')* TAU_BIG;

 for i = 1:length(Theta)
   fprintf('%s\t%f\n',Theta_Sym(i),Theta(i))
 end
