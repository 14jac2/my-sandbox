function [J] = Jacobian()

% DH parameters
d_i = [0 0 0];
a_i = [0 305 330+76]*1e-3;
al_i = [pi/2 0 0];

% Generate symbolic functions of the homogenous transformation amtrix
[T01,T02,T03,T0ee] = Homogenous_transforms(d_i,al_i,a_i);

syms A01 A02 A03 A0ee;
syms q1 q2 q3;         % joint Variables

A01 = T01(q1);
A02 = T02(q1,q2);
A03 = T03(q1,q2,q3);
A0ee = T0ee(q1,q2,q3);

z00 = [0;0;1];
z01 = A01(1:3,3);
z02 = A02(1:3,3);

p00 = 0; % IS THIS RIGHT?
p01 = A01(1:3,4);
p02 = A02(1:3,4);
p03 = A03(1:3,4);


J = [cross(z00,(p03-p00)) cross(z01,(p03-p01)) cross(z02,(p03-p02));
           z00                  z01                  z02           ];

end