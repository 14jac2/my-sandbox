figure (1)
subplot(2,1,1);
plot(e_PDFF(:,1), e_PDFF(:,2), 'b');
hold on
plot(e_PDFFCT(:,1), e_PDFFCT(:,2), 'r');
plot(e_invPDFF(:,1), e_invPDFF(:,2), 'c');
plot(e_invPDFFu(:,1), e_invPDFFu(:,2), 'm');
hold off
legend('PD + FF', 'PD + FF + CT', 'Inverse Dynamics', 'Inverse Dynamics with Uncertainty');
grid on
title('Joint 1 Angle Error');

subplot(2,1,2);
plot(e_PDFF(:,1), e_PDFF(:,3) , 'b');
hold on
plot(e_PDFFCT(:,1), e_PDFFCT(:,3) , 'r');
plot(e_invPDFF(:,1), e_invPDFF(:,3) , 'c');
plot(e_invPDFFu(:,1), e_invPDFFu(:,3) , 'm');
hold off
legend('PD + FF', 'PD + FF + CT', 'Inverse Dynamics', 'Inverse Dynamics with Uncertainty');
grid on
title('Joint 2 Angle Error');
