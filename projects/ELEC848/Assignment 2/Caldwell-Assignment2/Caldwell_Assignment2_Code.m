% Run this script to test all four controllers.

%% Simulating

% Different controllers
Robust = false; % Inverse dynamics with robuts (and sliding mode)
RobustSoft = false; % Inverse dynamics with robust and hard switch (and sliding mode)
SMSoft = false; % Sliding mode with soft switch
SlotineLi = false; % Slotine and Lie controller

% Actual dynamics
I = 10; 
MgL = 10; 

% Estimated dynamics
I_hat = 5; 
MgL_hat = 5; 

model = 'Assignment3';
t_max = 25;
dt_max = 0.01;

% The simulation will run for each configuration
% Controller 1: Iverse Dynamics
simdata1 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% Controller 2: Robust Inverse Dynamics
Robust = true;
simdata2 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% Controller 3: Robust Inverse Dynamics with soft switch
RobustSoft = true;
simdata3 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% Controller 4: Sliding mode with soft
SMSoft = true;
simdata4 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

% Controller 4: Slotine and Li
SlotineLi = true;
simdata5 = sim(model, 'StopTime', string(t_max), 'MaxStep', string(dt_max));

%% Plotting
% Inverse Dynamics
figure (1)
subplot(3,2,1);
plot(simdata1.q_d.time(:), simdata1.q_d.signals(2).values(:, 1), 'r');
hold on
plot(simdata1.q_d.time(:), simdata1.q_d.signals(1).values(:, 1), 'b');
hold off
grid on
legend('Desired', 'Actual');
title('Joint 1 Angle');

subplot(3,2,2);
plot(simdata1.q_d.time(:), simdata1.q_d.signals(2).values(:, 2), 'r');
hold on
plot(simdata1.q_d.time(:), simdata1.q_d.signals(1).values(:, 2), 'b');
hold off
grid on
legend('Desired', 'Actual');
title('Joint 2 Angle');

subplot(3,2,3);
plot(simdata1.e(:,1), simdata1.e(:,2) , 'b');
grid on
title('Joint 1 Angle Error');

subplot(3,2,4);
plot(simdata1.e_inv(:,1), simdata1.e_inv(:,3) , 'b');
grid on
title('Joint 2 Angle Error');

subplot(3,2,5);
plot(simdata1.Control(:,1), simdata1.Control(:,2) , 'b');
grid on
title('Joint 1 Control Input');

subplot(3,2,6);
plot(simdata1.Control(:,1), simdata1.Control(:,3) , 'b');
grid on
title('Joint 2 Control Input');
sgtitle('PD + FF Results');

% Plot FF + PD + CT controller results
% figure (2)
% subplot(3,2,1);
% plot(simdata2.q_d.time(:), simdata2.q_d.signals(2).values(:, 1), 'r');
% hold on
% plot(simdata2.q_d.time(:), simdata2.q_d.signals(1).values(:, 1), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint 1 Angle');
% 
% subplot(3,2,2);
% plot(simdata2.q_d.time(:), simdata2.q_d.signals(2).values(:, 2), 'r');
% hold on
% plot(simdata2.q_d.time(:), simdata2.q_d.signals(1).values(:, 2), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint 2 Angle');
% 
% subplot(3,2,3);
% plot(simdata2.e(:,1), simdata2.e(:,2) , 'b');
% grid on
% title('Joint 1 Angle Error');
% 
% subplot(3,2,4);
% plot(simdata2.e(:,1), simdata2.e(:,3) , 'b');
% grid on
% title('Joint 2 Angle Error');
% 
% subplot(3,2,5);
% plot(simdata2.Control(:,1), simdata2.Control(:,2) , 'b');
% grid on
% title('Joint 1 Control Input');
% 
% subplot(3,2,6);
% plot(simdata2.Control(:,1), simdata2.Control(:,3) , 'b');
% grid on
% title('Joint 2 Control Input');
% sgtitle('PD + FF + CT Results');
% 
% % Plot Inverse dynamics controller results
% figure (3)
% subplot(3,2,1);
% plot(simdata3.q_d.time(:), simdata3.q_d.signals(2).values(:, 1), 'r');
% hold on
% plot(simdata3.q_d.time(:), simdata3.q_d.signals(1).values(:, 1), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint 1 Angle');
% 
% subplot(3,2,2);
% plot(simdata3.q_d.time(:), simdata3.q_d.signals(2).values(:, 2), 'r');
% hold on
% plot(simdata3.q_d.time(:), simdata3.q_d.signals(1).values(:, 2), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint 2 Angle');
% 
% subplot(3,2,3);
% plot(simdata3.e_inv(:,1), simdata3.e_inv(:,2) , 'b');
% grid on
% title('Joint 1 Angle Error');
% 
% subplot(3,2,4);
% plot(simdata3.e_inv(:,1), simdata3.e_inv(:,3) , 'b');
% grid on
% title('Joint 2 Angle Error');
% 
% subplot(3,2,5);
% plot(simdata3.Control(:,1), simdata3.Control(:,2) , 'b');
% grid on
% title('Joint 1 Control Input');
% 
% subplot(3,2,6);
% plot(simdata3.Control(:,1), simdata3.Control(:,3) , 'b');
% grid on
% title('Joint 2 Control Input');
% sgtitle('Inverse Dynamics + PD + FF Results');
% 
% % Plot Inverse dynamics controller with uncertainty results
% figure (4)
% subplot(3,2,1);
% plot(simdata4.q_d.time(:), simdata4.q_d.signals(2).values(:, 1), 'r');
% hold on
% plot(simdata4.q_d.time(:), simdata4.q_d.signals(1).values(:, 1), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint 1 Angle');
% 
% subplot(3,2,2);
% plot(simdata4.q_d.time(:), simdata4.q_d.signals(2).values(:, 2), 'r');
% hold on
% plot(simdata4.q_d.time(:), simdata4.q_d.signals(1).values(:, 2), 'b');
% hold off
% grid on
% legend('Desired', 'Actual');
% title('Joint 2 Angle');
% 
% subplot(3,2,3);
% plot(simdata4.e_inv(:,1), simdata4.e_inv(:,2) , 'b');
% grid on
% title('Joint 1 Angle Error');
% 
% subplot(3,2,4);
% plot(simdata4.e_inv(:,1), simdata4.e_inv(:,3) , 'b');
% grid on
% title('Joint 2 Angle Error');
% 
% subplot(3,2,5);
% plot(simdata4.Control(:,1), simdata4.Control(:,2) , 'b');
% grid on
% title('Joint 1 Control Input');
% 
% subplot(3,2,6);
% plot(simdata4.Control(:,1), simdata4.Control(:,3) , 'b');
% grid on
% title('Joint 2 Control Input');
% sgtitle('Inverse Dynamics with 50% uncertainty + PD + FF Results');
% 
% % Finally, plot all the errors in one plot
% figure (5)
% subplot(2,1,1);
% plot(simdata1.e(:,1), simdata1.e(:,2) , 'b');
% hold on
% plot(simdata2.e(:,1), simdata2.e(:,2) , 'r');
% plot(simdata3.e_inv(:,1), simdata3.e_inv(:,2) , 'c');
% plot(simdata4.e_inv(:,1), simdata4.e_inv(:,2) , 'm');
% hold off
% legend('PD + FF', 'PD + FF + CT', 'Inverse Dynamics', 'Inverse Dynamics with Uncertainty');
% grid on
% title('Joint 1 Angle Error');
% 
% subplot(2,1,2);
% plot(simdata1.e(:,1), simdata1.e(:,3) , 'b');
% hold on
% plot(simdata2.e(:,1), simdata2.e(:,3) , 'r');
% plot(simdata3.e_inv(:,1), simdata3.e_inv(:,3) , 'c');
% plot(simdata4.e_inv(:,1), simdata4.e_inv(:,3) , 'm');
% hold off
% legend('PD + FF', 'PD + FF + CT', 'Inverse Dynamics', 'Inverse Dynamics with Uncertainty');
% grid on
% title('Joint 2 Angle Error');
% sgtitle('Controller Errors');
