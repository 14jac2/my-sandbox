figure (1)
subplot(4,2,1);
plot(V(:,1), V(:,2));
ylim([-50 600]);
grid on
title('Voltage Applied to Motor 1');

subplot(4,2,2);
plot(V(:,1), V(:,3));
ylim([-50 600]);
grid on
title('Voltage Applied to Motor 2');

subplot(4,2,3);
plot(T(:,1), T(:,2));
ylim([0 110]);
grid on
title('Torque Applied to Joint 1');

subplot(4,2,4);
plot(T(:,1), T(:,3));
ylim([0 110]);
grid on
title('Torque Applied to Joint 2');

subplot(4,2,5);
plot(q(:,1), q(:,2));
ylim([-1.6 0.1]);
grid on
title('Position of Joint 1');

subplot(4,2,6);
plot(q(:,1), q(:,3));
ylim([-1.6 0.1]);
grid on
title('Position of Joint 2');

subplot(4,2,7);
plot(q_dot(:,1), q_dot(:,2));
ylim([-0.5 0.5]);
grid on
title('Velocity of Joint 1');

subplot(4,2,8);
plot(q_dot(:,1), q_dot(:,3));
ylim([-0.5 0.5]);
grid on
title('Velocity of Joint 2');
